


/* Blocks for fixation, display of the pattern, then answer */
var fixation = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: '<div style="font-size:60px;margin-left: auto; margin-right: auto;margin-top: -36px;">+</div>',
  choices: "NO_KEYS",
  trial_duration: 200,
};

  /* With only one pattern */
var showPattern = { // this is a sort of a plugin isolated to manipulate the grating stimulus. 
  type: jsPsychHtmlThenFunction,
  stimulus: getHTMLCanvas(),
  trial_duration: 500,
  func: getEasyStim // defined in grating.js
};

var showMasking = { // this is a sort of a plugin isolated to manipulate the masking stimulus. 
  type: jsPsychHtmlThenFunction,
	stimulus: getHTMLCanvas(),
	trial_duration: 300,
	func: getStimCircle // defined in grating.js
};

  /* With 2 concurrent patterns */
  
function ShowCue(){
	let cueData = WithinSetCondition[indexWithinSetCondition];
	console.log( "Cue: " + cueData);
	
	let cue = "--";
	if( cueData[1] == "V" )
		cue = cueData[0] == 'L' ? "←" : "→";
	else if( cueData[1] == "I" )
		cue = cueData[0] == 'R' ? "←" : "→";
	document.getElementById("cueContainer").innerHTML = cue;
};

function ShowCueingInstruction(){
	document.getElementById("cueContainer").innerHTML = ( WithinSetCondition[indexWithinSetCondition][0] == 'L' ? "←" : "→" );
};
  
var cueing = {
    type: jsPsychHtmlThenFunction,
    stimulus: '<div id="cueContainer" style="font-size:60px;margin-left: auto; margin-right: auto;margin-top: -36px;"></div>',
    trial_duration: 300,
	func: ShowCue 
};

var showPatternDouble = { // this is a sort of a plugin isolated to manipulate the grating stimulus. 
  type: jsPsychHtmlThenFunction,
  stimulus: getHTMLCanvasDouble(),
  trial_duration: 500,
  func: getEasyStimDouble // defined in grating.js
};

var showMaskingDouble = { // this is a sort of a plugin isolated to manipulate the masking stimulus. 
  type: jsPsychHtmlThenFunction,
	stimulus: getHTMLCanvasDouble(),
	trial_duration: 300,
	func: getStimCircleDouble // defined in grating.js
};

var cueingInstruction = {
  type: jsPsychHtmlThenFunction,
  stimulus: '<div id="cueContainer" style="font-size:60px;margin-left: auto; margin-right: auto;margin-top: -36px;"></div>',
  trial_duration: 300,
	func: ShowCueingInstruction 
};


  /* For the answer */
function Show_orientation_bar(param){ // function used in the "answer" variable.
    var html = '<div style="display: block; position: absolute; top: 50%; margin-top:-36px; background-color: white; '+
    'width: 300px; height: 10px; transform : rotate('+(-param)+'deg);"></div>';
    return html;
}

function GetAdditionalStyleForAnswer(){ 
	if(indexCondition == 0)
		return "";
	if(WithinSetCondition[indexWithinSetCondition][0] == 'L')
		return "margin-left: -50%;";
	else
		return "margin-right: -50%;";
}

var incrementExpStage = { 
  type: jsPsychCallFunction,
  func: function() { 
    indexExpStage++;
	}
};
function GetExpStage(){ return experiment_stage[ indexExpStage ]; }

var last_answer;
function SetLastAnswer( _lastAnswer ){ 
  last_answer = _lastAnswer; 
}

var answer = {
    type: jsPsychOrientationMouseRespKeyboard,
    stim_function: Show_orientation_bar,
    condition_function: GetCondition,
    WithinSet_condition_function: GetWithinSetCondition,
    correct_response_function: GetAnglePattern,
    incorrect_response_function: GetAnglePatternSecondary,
    starting_value_function: GetAngleBar,
    set_answer_function: SetLastAnswer,
    adapted_constrast_function: GetAdaptedContrasr,
    angle_condition_function: GetTypeOfAngle,
    Experiment_Stage_function: GetExpStage,
    get_additional_style_function: GetAdditionalStyleForAnswer,
    button_label: `Sorry, I got distracted! - Let's skip this one <Br/> (to use only if you did not see the grid at all)`,
	data: {
        task: 'response'
      },
      
}

/* Compute the response error to be used in the stair case */
var Err = 0;
var ComputeErr = { 
  type: jsPsychCallFunction,
  func: function() { Err = anglePattern - last_answer ; 
    if (Err < -90){Err = 180 + anglePattern - last_answer };
    if (Err > 90){Err = - 180 + anglePattern - last_answer };
  console.log( "New err: " + Err ); 
} 
};
function GetErr(){ return Err; }

/* Staircase: 2 up 1 down */
var AdaptableContrast = 0.06
var correct_counter = 0
var Inside = 0
var Outside = 0

function afterTrialUpdate (){ 
  if(calibration == false)
    return;
  if (Math.abs(Err) > 10) {
    correct_counter = 0;
    AdaptableContrast += 0.005;
    Outside+=1;
  } else {
      correct_counter += 1;
      Inside += 1;
        if (correct_counter == 2) {
          AdaptableContrast -= 0.005;
        correct_counter = 0;
        }
     }
     
  console.log( "New contrast: " + AdaptableContrast + " How many correct " + correct_counter);
  console.log( "Inside: " + Inside + " Outside " + Outside);
}
function GetAdaptedContrasr(){ return AdaptableContrast; }

var UpdateForStaircase = {
  type: jsPsychCallFunction,
  func: afterTrialUpdate,
}

var FinalContrast= { 
	type: jsPsychCallFunction,
	func: function() { 
    AdaptableContrast = (AdaptableContrast * 1.2); 
		console.log( "New contrast: " + AdaptableContrast ); 
    return AdaptableContrast; 
	  }
  };