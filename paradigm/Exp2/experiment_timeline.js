
///////////////////////////////////////// Experiment Procedure Timeline //////////////////////////////
/* create experiment definition array */ 

var procedure_Calibration = [fixation, resetAngle, incrementIndexJitterArray, showPattern, showMasking, answer, ComputeErr, UpdateForStaircase];
var procedure_MainTask = [fixation, resetAngle, incrementIndexJitterArray, showPattern, showMasking, answer, ComputeErr];
var procedure_MainTask_full = [fixation, cueing, resetAngle, incrementIndexJitterArray, showPatternDouble, showMaskingDouble , answer, ComputeErr, incrementIndexWithinSetCondition];

if (Test == 1) {
  var n_repetition_same_angle = 1;
  var n_repetition_angle_type = 1; 
  var n_repetition_same_angle_meta = 4; 
  var n_repetition_angle_type_meta = 9; 
} 
else {
	var n_repetition_same_angle = 1;
	var n_repetition_angle_type = 1; //10
	var n_repetition_same_angle_meta = 4;
	var n_repetition_angle_type_meta = 18;
}


if (Test==0) // the calibration block will exist only if I am in the main experiment and not the test version
{	/* calibration phase - no metacognition needed - training */
	timeline.push(...procedure_Calibration);

		/* calibration phase - no metacognition needed - test */
	timeline.push(BeginCalibration);

	function BuildTimeline_Calibration()
	{
		for (var z = 0; z < n_repetition_angle_type; z++) 
		{ 
		  timeline.push(shuffleAnglesArray); 
		  for (var i = 0; i < anglesArray.length; i++) 
		  {
			for (var j = 0; j < n_repetition_same_angle; j ++) 
			{timeline.push(...procedure_Calibration);
			}
		  timeline.push(incrementIndexAnglesArray); 
		  }
		}
	}

	BuildTimeline_Calibration();

	timeline.push(HalfWay);
	BuildTimeline_Calibration();
}

if (Test==0)
{
	/* Main phase - metacognition needed - training */
	timeline.push(incrementExpStage);
	timeline.push(instructions_Metacog);
	timeline.push(instructions_Metacog_2);
	timeline.push(shuffleAnglesArray); 

	for (var j = 0; j < n_repetition_same_angle_meta; j++) {
	  timeline.push(...procedure_MainTask);
	}
	timeline.push(incrementIndexAnglesArray);
	timeline.push(metaconfiance2);

	

}
/* Main phase full - metacognition needed - instruction + training */
timeline.push(shuffleConditionCue); 
timeline.push(DecidedWithinSetCondition); 
timeline.push(instructions_Metacog_3);
timeline.push(FinalContrast);

for (var j = 0; j < n_repetition_same_angle_meta; j++) {
  timeline.push(...procedure_MainTask_full);
}
timeline.push(incrementIndexAnglesArray);
timeline.push(metaconfiance2);

/* Main phase - metacognition needed - test */
timeline.push(shuffleConditionCue); 
timeline.push(Begin_Metacog);


function BuildTimeline_MainTask()
{for (var z = 0; z < n_repetition_angle_type_meta; z++) 
  { 
    timeline.push(shuffleAnglesArray); 

    for (var i = 0; i < anglesArray.length; i++) 
    {timeline.push(DecidedWithinSetCondition); 
      for (var j = 0; j < n_repetition_same_angle_meta; j ++) 
      {
        timeline.push(...procedure_MainTask_full);
      }
    timeline.push(incrementIndexAnglesArray); 
    timeline.push(metaconfiance2); 
	
	
    }
  }
}


BuildTimeline_MainTask();

if (Test==0) // the last 3 blocks will exist only if I am in the main experiment and not the test version
{timeline.push(OneFourth);
  BuildTimeline_MainTask();
  timeline.push(TwoFourth);
  BuildTimeline_MainTask();
  timeline.push(ThreeFourth);
  BuildTimeline_MainTask();
}



/* define debrief */
var debrief_block = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: function(){
	  html = "Nearly finish! <Br/>Your responses are being saved. Press the space bar one more time";
    
    var now = new Date();
    
    var trials = jsPsych.data.get().filter({ task: 'response' });
    if (Test == 1) {
      trials.localSave('csv', 'save/result-' + now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate() + '_' + Math.round(Math.random() * 1000) + '.csv');
    }
	  
    return html;
  }
};
timeline.push(debrief_block);





/* finish connection with pavlovia.org */
if (Test==0){
var pavlovia_finish = {
  type: jsPsychPavlovia,
  command: "finish",
  participantId: "MetaAngleS"
};
timeline.push(pavlovia_finish);
}



var ClosingWindow = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: function(){
	  html = "Thanks a lot ! Here is the <a href='https://app.prolific.co/submissions/complete?cc=3D5A28F2'>link</a> to follow to get your compensation . <Br/>You can now close this window";
    
    return html;
  }
};
timeline.push(ClosingWindow); 