//////////////////////////////////* Initialization *////////////////////////////////////////
/* Initialize the experiment */
var jsPsych = initJsPsych({
  on_finish: function () {
    jsPsych.data.displayData();
  }
});

/* create timeline */
var timeline = [];

/* create a Test variable, to be able to test a "straight to data" version */
// Test = 1 --> test is on / Test = 0 --> main exp is on
var Test = 0;

/* init connection with pavlovia.org */
if (Test == 0) {
  var pavlovia_init = {
    type: jsPsychPavlovia,
    command: "init"
  };
  timeline.push(pavlovia_init);
}

/* create and display a progress bar */
var jsPsych = initJsPsych({
  show_progress_bar: true
});

/* preload images */
var preload = {
  type: jsPsychPreload,
  images: ['ConsentForm_Exp1.png']
};
timeline.push(preload);

/* Get navigator and monitor info */
var InfoNavigator = {
  type: jsPsychCallFunction,
  func: function () {
    console.log("Version Nov 7th ");
  },
  data: {
    task: 'response',
    NavigatorName: navigator.appCodeName + " " + navigator.appVersion,
  },
};

var InfoResolution = {
  type: jsPsychCallFunction,
  func: function () {
    console.log("Your screen resolution is: " + screen.width + "x" + screen.height);
  },
  data: {
    task: 'response',
    Resolution: screen.width + "x" + screen.height,
  },
}

/* define welcome message trial and enter full screen */ // !!!! DO NOT WORK WITH SAFARI 
var enter_fullscreen = {
  type: jsPsychFullscreen,
  fullscreen_mode: true
}
timeline.push(enter_fullscreen, InfoNavigator, InfoResolution);


//////////////////////////////////* Instructions *////////////////////////////////////////
/* Introduction to the experiment */

/* Consent */
var Consent = {
  type: jsPsychConsentForm,
  stimulus: 'ConsentForm_Exp1.png',
  choices: ['Continue', 'Stop'],
  stimulus_height:  screen.height*0.80, //950,
  maintain_aspect_ratio: true,
  prompt: `
  <p> By clicking on <strong>« continue »</strong> I certify that I have read <br/> and understood the information provided.</p> 
  <p> I know that I can withdraw from this experiment at any time,<br/> simply by closing the experiment window. </p>
  <p> I can ask a copy of this consent form by contacting <br/> Marie Chancel on Prolific internal mailing system. </p>
  `,
  data: {
    task: 'response'
  },
};
if (Test==0)
	timeline.push(Consent);

/* "Half way there" message for the calibration phase */
var HalfWay = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "The first part of the experiment is half done! <br/>Press any key when you are ready to continue."
};

/* "Good job! you deserve a quick break" message for the main phase */
var OneFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "1/4 done! Press any key when you are ready to continue."
};

var TwoFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "2/4 done! Press any key when you are ready to continue."
};

var ThreeFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "3/4 done! Press any key when you are ready to continue."
};

////////////////////////////////////* Demographic questions *////////////////////////////////////////
// Skipped in the test version
if (Test==0){
	var ToStart = {
	  type: jsPsychHtmlKeyboardResponse,
	  stimulus: "<strong>Just a few questions before we get started</strong> <br/> Press any key when you are ready to continue."
	};
	timeline.push(ToStart);

	var sex = {
	  type: jsPsychSurveyMultiChoice,
	  questions: [
		  {prompt: "<strong>You identify as :</strong>",
		  options: ["Woman", "Man", "Other"],
		  required: true, horizontal: true
		}
	  ],
	  data: {
		task: 'response'
	  },
	};
	timeline.push(sex);

	var age = {
		type: jsPsychSurveyText,
		questions: [{prompt: "<strong>How old are you?</strong>", required : true}
	  ],
	  data: {
		task: 'response'
	  },
	};
	timeline.push(age);
}

var StructureExp = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>This study is divided in two phases</strong></p>
	<p><strong>Part 1 - Familiarization:</strong> a 6-minute phase to introduce to you the main task you will have to do with a short break in the middle.
  <br/><strong>Part 2 - Main experiment:</strong> a 30-minute phase, divided in 4 blocks for you to be able to take short breaks and stay focus. </p>
  <p>Let's start with phase 1! (press the space bar)</p>
  `,
};
if (Test==0)
	timeline.push(StructureExp);


//////////////////////////////////* Calibrations *////////////////////////////////////////
/* define instructions for the calibration */
var instructions = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/Calibration_Example.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['I got it!'],
  prompt: `
  <p>In the first part of the experiment, <strong>a grating pattern</strong> will appear on the screen. Focus on its <strong>orientation</strong>. Then <strong>a white bar</strong> will appear: click on it and drag it until it reaches the same orientation than the previously seen pattern. Try to be as <strong>accurate</strong> as possible</p>
  
  <p><strong>Careful:</strong> Circles are briefly displayed after the pattern and before the bar appears. Don't let them distract you! Keep the pattern orientation in mind!</p>
  
  <p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};
if (Test==0)
	timeline.push(instructions);

var BeginCalibration = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>Ready to go for real?</strong></p>
	<p>Press the space bar to start.</p>
  `,
  post_trial_gap: 1000 //Delay (in ms) of blank screen before we continue the flow.
};

