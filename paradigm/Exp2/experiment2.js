//////////////////////////////////* Initialization *////////////////////////////////////////
/* Initialize the experiment */
var jsPsych = initJsPsych({
  on_finish: function () {
    jsPsych.data.displayData();
  }
});

/* create timeline */
var timeline = [];

/* create a Test variable, to be able to test a "straight to data" version */
// Test = 1 --> test is on / Test = 0 --> main exp is on
var Test = 1;

/* init connection with pavlovia.org */
if (Test == 0) {
  var pavlovia_init = {
    type: jsPsychPavlovia,
    command: "init"
  };
  timeline.push(pavlovia_init);
}

/* create and display a progress bar */
var jsPsych = initJsPsych({
  show_progress_bar: true
});

/* preload images */
var preload = {
  type: jsPsychPreload,
  images: ['ConsentForm_Exp1.png']
};
timeline.push(preload);

/* Get navigator and monitor info */
var InfoNavigator = {
  type: jsPsychCallFunction,
  func: function () {
    console.log("Version Oct 20th ");
  },
  data: {
    task: 'response',
    NavigatorName: navigator.appCodeName + " " + navigator.appVersion,
  },
};

var InfoResolution = {
  type: jsPsychCallFunction,
  func: function () {
    console.log("Your screen resolution is: " + screen.width + "x" + screen.height);
  },
  data: {
    task: 'response',
    Resolution: screen.width + "x" + screen.height,
  },
}

/* define welcome message trial and enter full screen */ // !!!! DO NOT WORK WITH SAFARI 
var enter_fullscreen = {
  type: jsPsychFullscreen,
  fullscreen_mode: true
}
timeline.push(enter_fullscreen, InfoNavigator, InfoResolution);


//////////////////////////////////* Instructions *////////////////////////////////////////
/* Introduction to the experiment */

/* Consent */
var Consent = {
  type: jsPsychConsentForm,
  stimulus: 'ConsentForm_Exp1.png',
  choices: ['Continue', 'Stop'],
  stimulus_height:  screen.height*0.80, //950,
  maintain_aspect_ratio: true,
  prompt: `
  <p> By clicking on <strong>« continue »</strong> I certify that I have read <br/> and understood the information provided.</p> 
  <p> I know that I can withdraw from this experiment at any time,<br/> simply by closing the experiment window. </p>
  <p> I can ask a copy of this consent form by contacting <br/> Marie Chancel on Prolific internal mailing system. </p>
  `,
  data: {
    task: 'response'
  },
};
timeline.push(Consent);

/* "Half way there" message for the calibration phase */
var HalfWay = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "The first part of the experiment is half done! <br/>Press any key when you are ready to continue."
};

/* "Good job! you deserve a quick break" message for the main phase */
var OneFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "1/4 done! Press any key when you are ready to continue."
};

var TwoFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "2/4 done! Press any key when you are ready to continue."
};

var ThreeFourth = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "3/4 done! Press any key when you are ready to continue."
};

////////////////////////////////////* Demographic questions *////////////////////////////////////////
// Skipped in the test version
if (Test==0){
var ToStart = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: "<strong>Just a few questions before we get started</strong> <br/> Press any key when you are ready to continue."
};
timeline.push(ToStart);

var sex = {
  type: jsPsychSurveyMultiChoice,
  questions: [
      {prompt: "<strong>You identify as :</strong>",
      options: ["Woman", "Man", "Other"],
      required: true, horizontal: true
    }
  ],
  data: {
    task: 'response'
  },
};
timeline.push(sex);

var age = {
    type: jsPsychSurveyText,
    questions: [{prompt: "<strong>How old are you?</strong>", required : true}
  ],
  data: {
    task: 'response'
  },
};
timeline.push(age);
};

var StructureExp = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>This study is divided in two phases</strong></p>
	<p><strong>Part 1 - Familiarization:</strong> a 6-minute phase to introduce to you the main task you will have to do with a short break in the middle.
  <br/><strong>Part 2 - Main experiment:</strong> a 30-minute phase, divided in 4 blocks for you to be able to take short breaks and stay focus. </p>
  <p>Let's start with phase 1! (press the space bar)</p>
  `,
};
timeline.push(StructureExp);


//////////////////////////////////* Calibrations *////////////////////////////////////////
/* define instructions for the calibration */
var instructions = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/Calibration_Example.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['I got it!'],
  prompt: `
  <p>In the first part of the experiment, <strong>a grating pattern</strong> will appear on the screen. Focus on its <strong>orientation</strong>. Then <strong>a white bar</strong> will appear: click on it and drag it until it reaches the same orientation than the previously seen pattern. Try to be as <strong>accurate</strong> as possible</p>
  
  <p><strong>Careful:</strong> Circles are briefly displayed after the pattern and before the bar appears. Don't let them distract you! Keep the pattern orientation in mind!</p>
  
  <p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};
timeline.push(instructions);

var BeginCalibration = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>Ready to go for real?</strong></p>
	<p>Press the space bar to start.</p>
  `,
  post_trial_gap: 1000 //Delay (in ms) of blank screen before we continue the flow.
};

/* Set of variables, and blocs-function, to produce the target angles we want with a specific jitter. */
var condition = [ "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_V", "Left_N", "Left_N", "Left_N", "Left_I", "Left_I", "Left_I", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_V", "Right_N", "Right_I", "Right_N", "Right_I", "Right_N", "Right_I"];
var indexCondition = 0;
var proportion = [ 0.33, 0.085, 0.085, 0.33, 0.085, 0.085];
var absol = [47, 12, 13, 47, 12, 13];
var shuffleConditionCue = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexCondition = 0; 
		condition = jsPsych.randomization.shuffle(condition);
		console.log( "New order: " + condition + ". New average angle: " + condition[ indexCondition ] ); 
	}
};
var incrementIndexCondition = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexCondition++; 
    
		/*if( indexAnglesArray < anglesArray.length )
			console.log( "New average angle: " + anglesArray[ indexAnglesArray ]); */
	}
};
function GetTypeOfCondition(){ return condition[ indexCondition ]; }



var anglesArray = [ 0, 45, 90, 135 ];
var jitterArray = [ -5, -2.5, 2.5, 5 ];
var indexAnglesArray = 0;
var indexJitterArray = 0;
var anglePattern = 0;
var calibration = true;
var indexExpStage = 0;
var experiment_stage = ["calibration", "metacognition_align", "metacognition_judgment"];
function GetAnglePattern(){ return anglePattern; }

var shuffleAnglesArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexAnglesArray = 0; 
		anglesArray = jsPsych.randomization.shuffle(anglesArray);
		/*console.log( "New order: " + anglesArray + ". New average angle: " + anglesArray[ indexAnglesArray ] ); */
	}
};

var incrementIndexAnglesArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexAnglesArray++; 
    indexJitterArray = 0; 
    jitterArray = jsPsych.randomization.shuffle(jitterArray);
		/*if( indexAnglesArray < anglesArray.length )
			console.log( "New average angle: " + anglesArray[ indexAnglesArray ]); */
	}
};
function GetTypeOfAngle(){ return anglesArray[ indexAnglesArray ]; }

var incrementIndexJitterArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexJitterArray++; 
	}
};

var resetAngle = { 
  type: jsPsychCallFunction,
  //func: function() { anglePattern = anglesArray[indexAnglesArray] ; } /* Without jitter */ 
  //func: function() { anglePattern = anglesArray[indexAnglesArray] + Math.round( Math.random() * 10 ) - 5;  } /* random jitter between -5 and 5 */
  func: function() { anglePattern = anglesArray[indexAnglesArray] + jitterArray[indexJitterArray]; }, /* pseudorandom jitter between -5 and 5 */

  
};  

/* Blocks for fixation, display of the pattern, then answer */
var fixation = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: '<div style="font-size:60px;margin-left: auto; margin-right: auto;margin-top: -36px;">+</div>',
  choices: "NO_KEYS",
  trial_duration: 200,
};

  /* With only one pattern */
var showPattern = { // this is a sort of a plugin isolated to manipulate the grating stimulus. 
  type: jsPsychHtmlThenFunction,
  stimulus: getHTMLCanvas(),
  trial_duration: 500,
  func: getEasyStim
};

var showMasking = { // this is a sort of a plugin isolated to manipulate the masking stimulus. 
  type: jsPsychHtmlThenFunction,
	stimulus: getHTMLCanvas(),
	func: getStimCircle,
	trial_duration: 300,
};

  /* With 2 concurrent patterns */
var cueing = {
    type: jsPsychHtmlKeyboardResponse,
    stimulus: '<div style="font-size:120px;margin-left: auto; margin-right: auto;margin-top: -36px;">+</div>',
    choices: "NO_KEYS",
    trial_duration: 300,
  };

var showPattern2 = { // this is a sort of a plugin isolated to manipulate the grating stimulus. 
  type: jsPsychHtmlThenFunction2,
  stimulus: getHTMLCanvas(),
  trial_duration: 500,
  func: getEasyStim
};

var showMasking2 = { // this is a sort of a plugin isolated to manipulate the masking stimulus. 
  type: jsPsychHtmlThenFunction2,
	stimulus: getHTMLCanvas(),
	func: getStimCircle,
	trial_duration: 300,
};

var cueingInstruction = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: '<div style="font-size:120px;margin-left: auto; margin-right: auto;margin-top: -36px;">+</div>',
  choices: "NO_KEYS",
  trial_duration: 300,
};


  /* For the answer */
function Show_orientation_bar(param){ // function used in the "answer" variable.
    var html = '<div style="display: block; position: absolute; top: 50%; margin-top:-36px; background-color: white; '+
    'width: 300px; height: 10px; transform : rotate('+(-param)+'deg);"></div>';
    return html;
}

function GetAngleBar() {
  angleBar = Math.round(Math.random() * 180);
  if (Math.abs(angleBar - anglePattern) > 10) { return angleBar; } 
  else if (angleBar  > 90) { return angleBar - 10; }
  else if (angleBar  < 90) { return angleBar + 10; }
}

var incrementExpStage = { 
  type: jsPsychCallFunction,
  func: function() { 
    indexExpStage++;
	}
};
function GetExpStage(){ return experiment_stage[ indexExpStage ]; }

var last_answer;
function SetLastAnswer( _lastAnswer ){ 
  last_answer = _lastAnswer; 
}

var answer = {
    type: jsPsychOrientationMouseRespKeyboard,
    stim_function: Show_orientation_bar,
    correct_response_function: GetAnglePattern,
    starting_value_function: GetAngleBar,
    set_answer_function: SetLastAnswer,
    adapted_constrast_function: GetAdaptedContrasr,
    angle_condition_function: GetTypeOfAngle,
    Experiment_Stage_function: GetExpStage,
    button_label: `Sorry, I got distracted! - Let's skip this one <Br/> (to use only if you did not see the grid at all)`,
	data: {
        task: 'response'
      },
      
}

/* Compute the response error to be used in the stair case */
var Err = 0;
var ComputeErr = { 
  type: jsPsychCallFunction,
  func: function() { Err = anglePattern - last_answer ; 
    if (Err < -90){Err = 180 + anglePattern - last_answer };
    if (Err > 90){Err = - 180 + anglePattern - last_answer };
  console.log( "New err: " + Err ); 
} 
};
function GetErr(){ return Err; }

/* Staircase: 2 up 1 down */
var AdaptableContrast = 0.06
var correct_counter = 0
var Inside = 0
var Outside = 0

function afterTrialUpdate (){ 
  if(calibration == false)
    return;
  if (Math.abs(Err) > 10) {
    correct_counter = 0;
    AdaptableContrast += 0.005;
    Outside+=1;
  } else {
      correct_counter += 1;
      Inside += 1;
        if (correct_counter == 2) {
          AdaptableContrast -= 0.005;
        correct_counter = 0;
        }
     }
     
  console.log( "New contrast: " + AdaptableContrast + " How many correct " + correct_counter);
  console.log( "Inside: " + Inside + " Outside " + Outside);
}
function GetAdaptedContrasr(){ return AdaptableContrast; }

var UpdateForStaircase = {
  type: jsPsychCallFunction,
  func: afterTrialUpdate,
}


///////////////////////////////////////// Metacognitive Part //////////////////////////////

var instructions_Metacog = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Explanation.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Next'],
  prompt: `
  <p><strong>Time to move on to the main experiment! </strong></p>
 
  <p>Your task remains the same: a grating pattern will appear on the screen. Focus on its <strong>orientation</strong>.	Then a white bar will appear: click on it and drag it until it reaches the same orientation than the previously seen pattern. Try to be as <strong>accurate</strong> as possible</p>

  <p>Once you have done <strong>several trials</strong> in a row, you will have to report how well you think you performed <strong>across these trials</strong>. Let's consider the mean target orientation, how precise were you in matching it? </p>
  
  
  <p>Play the video on the right to see the tutorial then press Next.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var instructions_Metacog_2 = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Example.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Got it!'],
  prompt: `
  <p>Use your mouse to adjust 
  <br/> 1 - the circle until the central bar orientation matches the <strong>mean target orientation</strong>.
  <br/> 2 - the red area until it fits your <strong> zone of confidence</strong> (= how far from the correct orientation you think you may have drifted in the previous trials). </p>
	<p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var instructions_Metacog_3 = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Example.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Got it!'],
  prompt: `
  <p>Easy right? Well, let's make it a little more difficult! </p>
  <p> Now <strong>2 grating patterns </strong> are going to appear each time. Remember <strong>both</strong>!
  <br/> An arrow  will appear next to let you know <strong> which one is the real target </strong> (= the orientation you have to match). </p>
	<br/> Tips: the arrow that appear before the pattern will give you a cue, but not always a valid one!
  <p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var Begin_Metacog={
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>Ready to go for real?</strong></p>
	<p>Press the space bar to start.</p>
  `,
  post_trial_gap: 1000 //Delay (in ms) of blank screen before we continue the flow.
};

function Metaconfiance(param){ // function used by answer.
	param = 90 - param;
  var html = '<img src="img/metaconfiance.png" style="position:absolute; width:100%; left:0px;" ><div class="circle">'
+'<div class="slice" style = "transform: rotate(90deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: scaleX(-1) rotate(90deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: rotate(270deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: scaleX(-1) rotate(270deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="lineMarker"> </div>'
+'<div>';
  return html;
}

function GetAngle90Random(){ return  1.5 ; } // Not relevant anymore since the starting value for the zone of confidance now depends on the participant reported mean target orientation but we still need a default value.

function Show_orientation_bar2(param){ // function used by answer.
  var html = '<img src="img/metaconfiance.png" style="position:absolute; width:100%; left:0px;'+
  'transform : rotate('+(-param)+'deg);" />';
  return html;
}

var metaconfiance2 = {
  type: jsPsychMetaconfiance_MeanSD,
  stim_orientation_function: Show_orientation_bar2,
  stim_metaconfiance_function: Metaconfiance,
  starting_value_orientation_function: GetAngleBar,
  starting_value_metaconfiance_function: GetAngle90Random,
data: {
      task: 'response'
    },
}

///////////////////////////////////////// Experiment Procedure Timeline //////////////////////////////
/* create experiment definition array */

var procedure_Calibration = [fixation, resetAngle, incrementIndexJitterArray, showPattern, showMasking, answer, ComputeErr, UpdateForStaircase];
var procedure_MainTask = [fixation, resetAngle, incrementIndexJitterArray, showPattern, showMasking, answer, ComputeErr];
var procedure_MainTask_full = [fixation, incrementIndexCondition, cueing, resetAngle, incrementIndexJitterArray, showPattern2, showMasking2, cueingInstruction , answer, ComputeErr];

if (Test == 1) {
  var n_repetition_same_angle = 1;
  var n_repetition_angle_type = 1; 
  var n_repetition_same_angle_meta = 2; 
  var n_repetition_angle_type_meta = 1; 
} else {var n_repetition_same_angle = 1;
        var n_repetition_angle_type = 10;
        var n_repetition_same_angle_meta = 4;
        var n_repetition_angle_type_meta = 9;
      }


if (Test==0) // the calibration block will exist only if I am in the main experiment and not the test version
{	/* calibration phase - no metacognition needed - training */
timeline.push(...procedure_Calibration);

	/* calibration phase - no metacognition needed - test */
timeline.push(BeginCalibration);

function BuildTimeline_Calibration()
{for (var z = 0; z < n_repetition_angle_type; z++) 
    { 
      timeline.push(shuffleAnglesArray); 
      for (var i = 0; i < anglesArray.length; i++) 
      {
        for (var j = 0; j < n_repetition_same_angle; j ++) 
        {timeline.push(...procedure_Calibration);
        }
      timeline.push(incrementIndexAnglesArray); 
      }
    }
}

BuildTimeline_Calibration();

timeline.push(HalfWay);
  BuildTimeline_Calibration();
}


/* Main phase - metacognition needed - training */
timeline.push(incrementExpStage);
timeline.push(instructions_Metacog);
timeline.push(instructions_Metacog_2);
timeline.push(shuffleAnglesArray); 
timeline.push(shuffleConditionCue); 

for (var j = 0; j < n_repetition_same_angle_meta; j++) {
  timeline.push(...procedure_MainTask);
}
timeline.push(incrementIndexAnglesArray);
timeline.push(metaconfiance2);

/* Main phase full - metacognition needed - instruction + training */
timeline.push(instructions_Metacog_3);

/* Main phase - metacognition needed - test */
timeline.push(Begin_Metacog);
function BuildTimeline_MainTask()
{for (var z = 0; z < n_repetition_angle_type_meta; z++) 
  { 
    timeline.push(shuffleAnglesArray); 

    for (var i = 0; i < anglesArray.length; i++) 
    {
      for (var j = 0; j < n_repetition_same_angle_meta; j ++) 
      {
        timeline.push(...procedure_MainTask_full);
      }
    timeline.push(incrementIndexAnglesArray); 
    timeline.push(metaconfiance2); 
    }
  }
}


BuildTimeline_MainTask();

if (Test==0) // the last 3 blocks will exist only if I am in the main experiment and not the test version
{timeline.push(OneFourth);
  BuildTimeline_MainTask();
  timeline.push(TwoFourth);
  BuildTimeline_MainTask();
  timeline.push(ThreeFourth);
  BuildTimeline_MainTask();
}



/* define debrief */
var debrief_block = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: function(){
	  html = "Nearly finish! <Br/>Your responses are being saved. Press the space bar one more time";
    
    var now = new Date();
    
    var trials = jsPsych.data.get().filter({ task: 'response' });
    if (Test == 1) {
      trials.localSave('csv', 'save/result-' + now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate() + '_' + Math.round(Math.random() * 1000) + '.csv');
    }
	  
    return html;
  }
};
timeline.push(debrief_block);





/* finish connection with pavlovia.org */
if (Test==0){
var pavlovia_finish = {
  type: jsPsychPavlovia,
  command: "finish",
  participantId: "MetaAngleS"
};
timeline.push(pavlovia_finish);
}



var ClosingWindow = {
  type: jsPsychHtmlKeyboardResponse,
  stimulus: function(){
	  html = "Thanks a lot ! Here is the <a href='https://app.prolific.co/submissions/complete?cc=3D5A28F2'>link</a> to follow to get your compensation . <Br/>You can now close this window";
    
    return html;
  }
};
timeline.push(ClosingWindow); 