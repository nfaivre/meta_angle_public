
/* Set of variables, and blocs-function, to produce the target angles we want with a specific jitter. 
 144 values : 94 with valid indice (3 valid 1 invalid), 25 with neutral (2-2), 25 with inverse (1 valid 3 invalid). Half of them left, half right. */
var condition = [];
for( let i = 0; i < 144; i++ )
{

	if( i < 47 )
		value = 'F';
	else if( i < 94 )
		value = 'V';
	else if( i < 119 )
		value = 'I';
	else
		value = 'N';
	condition.push(value);
}

var indexCondition = 0;
var shuffleConditionCue = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexCondition = 0; 
		condition = jsPsych.randomization.shuffle(condition);
		console.log( "New order: " + condition + ". New average angle: " + condition[ indexCondition ] ); 
	}
};

function GetTypeOfCondition(){ return condition[ indexCondition ]; }


var indexWithinSetCondition = 0;
var WithinSetCondition=[];

function SetWithSetCondition(){
	indexWithinSetCondition = 0;
	let setcond = condition[indexCondition];
	console.log( "setcond: " + setcond);
	if( setcond == "F" )
		WithinSetCondition = ["V", "V", "V", "V"];

	else if( setcond == "V" )
		WithinSetCondition = ["V", "V", "V", "I"];
		
	else if( setcond == "I" )
		WithinSetCondition = ["V", "I", "I", "I"];

	else if( setcond == "N" )
		WithinSetCondition = ["V", "V", "I", "I"];
	
	else
	console.log( "Debug: " + "pas bien" ); 

	for(let i = 0; i < 4; i++ )
		WithinSetCondition[i] = (Math.floor(( Math.random() * 2)) %2 == 0 ? 'L' : 'R'  ) + WithinSetCondition[i];


	WithinSetCondition = jsPsych.randomization.shuffle(WithinSetCondition)
	console.log( "New WithinSetCondition: " + WithinSetCondition ); 


	indexCondition++;
}

var DecidedWithinSetCondition = { 
	type: jsPsychCallFunction,
	func: SetWithSetCondition
  };

var incrementIndexWithinSetCondition = { 
	type: jsPsychCallFunction,
	func: function() { 
		indexWithinSetCondition++; 
	  
		console.log( "New condition now: " + WithinSetCondition[indexWithinSetCondition] ); 
	  }
  };
  function GetTypeOfWithinSetCondition(){ return WithinSetCondition[ indexWithinSetCondition ]; }

var anglesArray = [ 0, 90];
var jitterArray = [ -5, -2.5, 2.5, 5 ];
var indexAnglesArray = 0;
var indexJitterArray = 0;
var anglePattern = 0;
var anglePatternSecondary = 0;
var calibration = true;
var indexExpStage = 0;
var experiment_stage = ["calibration", "metacognition_align", "metacognition_judgment"];
function GetAnglePattern(){ return anglePattern; }
function GetAnglePatternSecondary(){ return anglePatternSecondary; }
function GetCondition(){ return condition[indexCondition-1]; }
function GetWithinSetCondition(){ return WithinSetCondition[indexWithinSetCondition]; }

var shuffleAnglesArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexAnglesArray = 0; 
		anglesArray = jsPsych.randomization.shuffle(anglesArray);
		/*console.log( "New order: " + anglesArray + ". New average angle: " + anglesArray[ indexAnglesArray ] ); */
	}
};

var incrementIndexAnglesArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexAnglesArray++; 
    indexJitterArray = 0; 
    jitterArray = jsPsych.randomization.shuffle(jitterArray);
		/*if( indexAnglesArray < anglesArray.length )
			console.log( "New average angle: " + anglesArray[ indexAnglesArray ]); */
	}
};
function GetTypeOfAngle(){ return anglesArray[ indexAnglesArray ]; }

var incrementIndexJitterArray = { 
  type: jsPsychCallFunction,
  func: function() { 
		indexJitterArray++; 
	}
};



var resetAngle = { 
  type: jsPsychCallFunction,
  //func: function() { anglePattern = anglesArray[indexAnglesArray] ; } /* Without jitter */ 
  //func: function() { anglePattern = anglesArray[indexAnglesArray] + Math.round( Math.random() * 10 ) - 5;  } /* random jitter between -5 and 5 */
  func: function() { 
		anglePattern = anglesArray[indexAnglesArray] + jitterArray[indexJitterArray];  /* pseudorandom jitter between -5 and 5 */
		anglePatternSecondary = anglesArray[indexAnglesArray] - (jitterArray[indexJitterArray] + Math.round( Math.random() * 3 ));  
	},
};  

function GetAngleBar() {
  angleBar = Math.round(Math.random() * 180);
  if (Math.abs(angleBar - anglePattern) > 10) { return angleBar; } 
  else if (angleBar  > 90) { return angleBar - 10; }
  else if (angleBar  < 90) { return angleBar + 10; }
}


