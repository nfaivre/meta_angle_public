var jsPsychOrientation = (function (jspsych) {
  'use strict';

  const info = {
      name: "orientation",
      parameters: {
          /** A function with a single parameter that returns an HTML-formatted string representing the stimulus. */
          stim_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Stimulus function",
              default: undefined,
          },
		  /** A function with no parameter that returns the value of the correct answer. */
          correct_response_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Get response function",
              default: undefined,
          },
          /** The starting value of the stimulus parameter. */
          starting_value_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },

         /** The grating contrast. */
           adapted_constrast_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "At what contrast is the gabor",
                default: undefined,
         },

          /** The Angle condition. */
            angle_condition_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "What type of angle (without the jitter)",
                default: undefined,
            },

          /** To stock the future last orientation. */
          set_answer_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Bar final orientation",
              default: undefined,
          },
          /** The change in the stimulus parameter caused by pressing one of the modification keys. */
          step_size: {
              type: jspsych.ParameterType.FLOAT,
              pretty_name: "Step size",
              default: 2,
          },
          /** The key to press for increasing the parameter value. */
          key_increase: {
              type: jspsych.ParameterType.KEY,
              pretty_name: "Key increase",
              default: 'ArrowRight',
          },
          /** The key to press for decreasing the parameter value. */
          key_decrease: {
              type: jspsych.ParameterType.KEY,
              pretty_name: "Key decrease",
              default: 'ArrowLeft',
          },
          /** The key to press for validating. */
          key_validate: {
              type: jspsych.ParameterType.KEY,
              pretty_name: "Key decrease",
              default: 'Enter',
          },
      },
  };
  /**
   * **orientation**
   *
   * jsPsych plugin for a orientation task where the subject recreates a stimulus from memory
   *
   * @author Josh de Leeuw/Gabriel Wink
   * @see inspired by {@link https://www.jspsych.org/plugins/jspsych-reconstruction/ reconstruction plugin documentation on jspsych.org}
   */
  class OrientationPlugin {
      constructor(jsPsych) {
          this.jsPsych = jsPsych;
      }
      trial(display_element, trial)
	  {
          const after_response = (info) => { // function
              //console.log('fire');
			  if (FirstKeyPress == 0){
                  FirstKeyPress = performance.now();
              }

              if (this.jsPsych.pluginAPI.compareKeys(info.key, trial.key_validate)) 
			  {
                  endTrial();
				  return;
              }
              var key_i = trial.key_increase;
              var key_d = trial.key_decrease;
              // get new param value
              if (this.jsPsych.pluginAPI.compareKeys(info.key, key_i)) {
                  param = param + trial.step_size;
              }
              else if (this.jsPsych.pluginAPI.compareKeys(info.key, key_d)) {
                  param = param - trial.step_size;
              }
              param = param%180;
			  if(param < 0)
				  param += 180;
              // refresh the display
              draw(param);
          };
		  
          const draw = (param) => { // function
              //console.log(param);
              div_container.innerHTML = trial.stim_function(param);
          };
		  
		  const endTrial = () => { // function
              // measure response time
              var endTime = performance.now();
              var response_time = Math.round(endTime - startTime);
              var RealResponseTime = Math.round(FirstKeyPress - startTime);
              // clear keyboard response
              this.jsPsych.pluginAPI.cancelKeyboardResponse(key_listener);
              // save data
              var trial_data = {
                  TypeOfAngle: trial.angle_condition_function(),
                  correct_response: trial.correct_response_function(),
                  response: param,
                  Err: trial.correct_response_function() - param,
                  Initial_position: trial.starting_value_function(),
                  RT: RealResponseTime,
                  AnswerDuration: response_time, 
                  Contrast: trial.adapted_constrast_function(),
                  
              };
              trial.set_answer_function(param);
              console.log( "Last answer: " + param ); 
              display_element.innerHTML = "";
              // next trial
              this.jsPsych.finishTrial(trial_data);
          };
		  
		  
          var FirstKeyPress = 0;
          var startTime = performance.now();
          // current param level
          var param = trial.starting_value_function();
		  display_element.innerHTML = '<div id="jspsych-orientation-stim-container"><div id="orientation_container" style="display: block; margin: auto; height: 300px; width: 300px; position: relative;">'
										+ '</div><p>Press left and right arrows to orient the bar.</p><p>When the angle is the same as the previous pattern, press enter </p></div>';
          
		  var div_container = document.getElementById("orientation_container");
		  
          // listen for responses
          var key_listener = this.jsPsych.pluginAPI.getKeyboardResponse({
              callback_function: after_response,
              valid_responses: [trial.key_increase, trial.key_decrease, trial.key_validate],
              rt_method: "performance",
              persist: true,
              allow_held_key: true,
          });
          // draw first iteration
          draw(param);
      }
  }
  OrientationPlugin.info = info;

  return OrientationPlugin;

})(jsPsychModule);
