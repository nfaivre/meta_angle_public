var jsPsychMetaconfiance = (function (jspsych) {
  'use strict';

  const info = {
      name: "metaconfiance",
      parameters: {
          /** A function with a single parameter that returns an HTML-formatted string representing the stimulus. */
          stim_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Stimulus function",
              default: undefined,
          },

        
          /** The starting value of the stimulus parameter. */
          starting_value_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },
		  
          /** The key to press for validating. */
          key_validate: {
            type: jspsych.ParameterType.KEY,
            pretty_name: "Key stop",
            default: ' ',
        },

          /** The HTML string to be displayed. */
          stimulus_text: {
              type: jspsych.ParameterType.HTML_STRING,
              pretty_name: "Stimulus text",
              default: "Click and drag to resize the portion of he circle corresponding to your zone of confidence.<br /> Then, press the space bar to continue",
          },

      },
  };
  /**
   * **metaconfiance**
   *
   * jsPsych plugin where the subject evaluate their performance, by setting a portion of a circle
   *
   * @author Josh de Leeuw/Gabriel Wink
   * @see inspired by {@link https://www.jspsych.org/plugins/jspsych-reconstruction/ reconstruction plugin documentation on jspsych.org}
   */
  class MetaconfiancePlugin {
      constructor(jsPsych) {
          this.jsPsych = jsPsych;
      }
      trial(display_element, trial)
	  {
          const draw = (param) => { // function
              //console.log(param);
              div_container.innerHTML = trial.stim_function(param);
          };
		  
		  const endTrial = () => { // function
              // measure response time
              if (FirstKeyPress == 0){
                FirstKeyPress = performance.now();
            }
              var endTime = performance.now();
              var response_time = Math.round(endTime - startTime);
              var RealResponseTime = Math.round(FirstKeyPress - startTime);
             
              // clear keyboard response
             this.jsPsych.pluginAPI.cancelKeyboardResponse(key_listener);
              // clear document event listeners
              document.removeEventListener("mouseup", mouseupevent);
			  document.removeEventListener("mousemove", resizeevent);
              // save data
              var Experiment_Stage="metacognition_judgment"
              var trial_data = {
                  ExpStage: Experiment_Stage,
                  metaconfiance: param,
                  Initial_position: trial.starting_value_function(),
                  RT: RealResponseTime,
                  AnswerDuration: response_time,
              };
			  
              display_element.innerHTML = "";
              // next trial
              this.jsPsych.finishTrial(trial_data);
          };
		  
		  
          var FirstKeyPress = 0;
          var startTime = performance.now();
          // current param level
          var param = trial.starting_value_function();
		  var width = 300;
		  display_element.innerHTML = '<div id="myModal" class="modal"> <div class="modal-content"> <span class="close">&times;</span> <p>Please adjust the confidence area before pressing the space bar</p> </div></div>'
                                        +'<div id="jspsych-metaconfiance-stim-container"><div id="metaconfiance_container" '
										+ 'style="display: block; margin-left: auto; margin-right: auto;margin-top: -36px; height: ' + width + 'px; width: ' + width + 'px; position: relative;"></div>'
										+ '<p>' + trial.stimulus_text + '</p>' 
										+ '</div>';
          		  
		  var div_container = document.getElementById("metaconfiance_container");
          var dragging = false;
		  div_container.addEventListener("mousedown", (e) => {
				  e.preventDefault();
				  dragging = true;
			  });
			  
		  var mouseupevent = (e) => { dragging = false; };
          document.addEventListener("mouseup", mouseupevent);
          var resizeevent = (e) => {
              if (dragging) {
				  if (FirstKeyPress == 0){
					  FirstKeyPress = performance.now();
				  }
				  var bounds = div_container.getBoundingClientRect();
				  var mouseX = e.clientX - bounds.left - width * 0.5;
				  var mouseY = e.clientY - bounds.top - width * 0.5;
				  param = -Math.atan( mouseY / mouseX ) * 180 / Math.PI;
				  if(param < 0)
					  param += 180;
				  if( param > 90 ) //For this, we want a result between 0 and 90.
					param = 180 - param;
					
                  //console.log( "angle: " + param + ". X: " + mouseX + ". Y: " + mouseY ); // use to validate by clicking on the button
				  draw(param);
              }
          };
          document.addEventListener("mousemove", resizeevent);
          draw(param);

          const after_response = (info) => {
             if (this.jsPsych.pluginAPI.compareKeys(info.key, trial.key_validate)) 
            {if (FirstKeyPress == 0){ // Get the modal
                var modal = document.getElementById("myModal");
                
                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];
                
                // open the modal
                modal.style.display = "block";
                
                 // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                  modal.style.display = "none";
                }
  
                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                  if (event.target == modal) {
                    modal.style.display = "none";
                  }
                }
            }
            else{
                endTrial();
                return;
            }
            }
          }

          var key_listener = this.jsPsych.pluginAPI.getKeyboardResponse({
            callback_function: after_response,
            valid_responses: [trial.key_validate],
            rt_method: "performance",
            persist: true,
            allow_held_key: true,
        });
      }
  }
  MetaconfiancePlugin.info = info;

  return MetaconfiancePlugin;

})(jsPsychModule);
