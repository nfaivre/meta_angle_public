var jsPsychOrientationMouseRespKeyboard = (function (jspsych) {
  'use strict';

  const info = {
      name: "orientation-mouse",
      parameters: {
          /** A function with a single parameter that returns an HTML-formatted string representing the condition. */
          condition_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "condition",
              default: undefined,
          },

          /** A function with a single parameter that returns an HTML-formatted string representing the condition within a set. */
          WithinSet_condition_function: {
            type: jspsych.ParameterType.FUNCTION,
            pretty_name: "condition within a set",
            default: undefined,
        },

           /** A function with a single parameter that returns an HTML-formatted string representing the stimulus. */
           stim_function: {
            type: jspsych.ParameterType.FUNCTION,
            pretty_name: "Stimulus function",
            default: undefined,
        },

		  /** A function with no parameter that returns the value of the correct answer. */
          correct_response_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Get response function",
              default: undefined,
          },

          /** A function with no parameter that returns the value of the incorrect answer. */
          incorrect_response_function: {
            type: jspsych.ParameterType.FUNCTION,
            pretty_name: "Get secondary angle ",
            default: undefined,
        },
          /** The starting value of the stimulus parameter. */
          starting_value_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },

         /** The grating contrast. */
           adapted_constrast_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "At what contrast is the gabor",
                default: undefined,
         },

          /** The Angle condition. */
            angle_condition_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "What type of angle (without the jitter)",
                default: undefined,
            },

          /** The Type of trial (calibration or metacog). */
            Experiment_Stage_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "What stage of the experiment",
              default: undefined,
          },

          /** To stock the future last orientation. */
          set_answer_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Bar final orientation",
              default: undefined,
          },

          /** Additional style for the bar container */
          get_additional_style_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Additional style for the bar container",
              default: undefined,
          },
		  
          /** The key to press for validating. */
          key_validate: {
            type: jspsych.ParameterType.KEY,
            pretty_name: "Key stop",
            default: ' ',
        },

          /** The text that appears on the button to finish the trial. */
          button_label: {
              type: jspsych.ParameterType.STRING,
              pretty_name: "Button label",
              default: "Continue",
          },

        
      },
  };
  /**
   * **orientation-mouse**
   *
   * jsPsych plugin for a orientation task where the subject recreates a stimulus from memory, using mouse
   *
   * @author Josh de Leeuw/Gabriel Wink
   * @see inspired by {@link https://www.jspsych.org/plugins/jspsych-reconstruction/ reconstruction plugin documentation on jspsych.org}
   */
  class OrientationMouseRespKeyboardPlugin {
      constructor(jsPsych) {
          this.jsPsych = jsPsych;
      }
      trial(display_element, trial)
	  {
          const draw = (param) => { // function
              //console.log(param);
              div_container.innerHTML = trial.stim_function(param);
          };
		  
          
          var flag = 'off';
          var InitialValue = 0;

		  const endTrial = () => { // function
              // measure response time
              if (FirstKeyPress == 0){
                FirstKeyPress = performance.now();
                }

              var Corr_Error = trial.correct_response_function() - param;
              
              if (Corr_Error > 90) { Corr_Error = trial.correct_response_function() - 180 - param;} 
                else if (Corr_Error < - 90) { Corr_Error = trial.correct_response_function() + 180 - param; }

              

              var endTime = performance.now();
              var response_time = Math.round(endTime - startTime);
              var RealResponseTime = Math.round(FirstKeyPress - startTime);
             
              // clear keyboard response
             this.jsPsych.pluginAPI.cancelKeyboardResponse(key_listener);
              // clear document event listeners
              document.removeEventListener("mouseup", mouseupevent);
			        document.removeEventListener("mousemove", resizeevent);

              

              // save data
              var trial_data = {
                  ExpStage: trial.Experiment_Stage_function(),
                  Condition: trial.condition_function(),
                  WithSetCondition: trial.WithinSet_condition_function(),
                  TypeOfAngle: trial.angle_condition_function(),
                  correct_response: trial.correct_response_function(),
                  incorrect_response: trial.incorrect_response_function(),
                  response: param,
                  Err: Corr_Error,
                  Initial_position: InitialValue,
                  RT: RealResponseTime,
                  AnswerDuration: response_time, 
                  Contrast: trial.adapted_constrast_function(),
                  Flag: flag
                  
                  
              };
              trial.set_answer_function(param);
              console.log( "Last answer: " + param ); 
              display_element.innerHTML = "";
              // next trial
              this.jsPsych.finishTrial(trial_data);
            };
		  
		  
          var FirstKeyPress = 0;
          var startTime = performance.now();
          // current param level
          var param = trial.starting_value_function();
          InitialValue = param;

		  var width = 300;
		  display_element.innerHTML = '<div id="myModal" class="modal"> <div class="modal-content"> <span class="close">&times;</span> <p>Please adjust the bar before pressing the space bar</p> </div></div>'
                    + '<div id="jspsych-orientation-mouse-stim-container"><div id="orientation_container" '
										+ 'style="display: block; margin: auto; height: ' + width + 'px; width: ' + width + 'px; position: relative; ' + ( trial.get_additional_style_function != undefined ? trial.get_additional_style_function() : '' ) + '"></div>'
										+ '<p> Click on the bar and drag it to change its orientation.<br /> When it matches the previous pattern, press the space bar </p>' 
										+ '<div><a class="jspsych-btn" id="jspsych-orientation-mouse-btn" style="margin-bottom: -150px">' + trial.button_label + '</a></div>';
          
		  


        // We want to flag the missed trials
		  const button = document.getElementById('jspsych-orientation-mouse-btn');

          button.addEventListener('click', function handleClick() {
            flag = 'on';
            console.log('element clicked');
            endTrial(); 
          }); 



		  var div_container = document.getElementById("orientation_container");
          var dragging = false;
		  div_container.addEventListener("mousedown", (e) => {
				  e.preventDefault();
				  dragging = true;
			  });
       
        

		  var mouseupevent = (e) => { dragging = false; };
          document.addEventListener("mouseup", mouseupevent);
          var resizeevent = (e) => {
              if (dragging) {
				  if (FirstKeyPress == 0){
					  FirstKeyPress = performance.now();
				  }
				  var bounds = div_container.getBoundingClientRect();
				  var mouseX = e.clientX - bounds.left - width * 0.5;
				  var mouseY = e.clientY - bounds.top - width * 0.5;
				  param = -Math.atan( mouseY / mouseX ) * 180 / Math.PI;
				  if(param < 0)
					  param += 180;
                  //console.log( "angle: " + param + ". X: " + mouseX + ". Y: " + mouseY ); // use to validate by clicking on the button
				  draw(param);
              }
          };
          document.addEventListener("mousemove", resizeevent);
          draw(param);

        const after_response = (info) => {
          if (this.jsPsych.pluginAPI.compareKeys(info.key, trial.key_validate)) {
            if (FirstKeyPress == 0) { 
              // Get the modal
              var modal = document.getElementById("myModal");
              
              // Get the <span> element that closes the modal
              var span = document.getElementsByClassName("close")[0];
              
              // open the modal
              modal.style.display = "block";
              
               // When the user clicks on <span> (x), close the modal
              span.onclick = function() {
                modal.style.display = "none";
              }

              // When the user clicks anywhere outside of the modal, close it
              window.onclick = function(event) {
                if (event.target == modal) {
                  modal.style.display = "none";
                }
              } 
            }
            else {
              endTrial();
              return;
            }
          }
        }





          var key_listener = this.jsPsych.pluginAPI.getKeyboardResponse({
            callback_function: after_response,
            valid_responses: [trial.key_validate],
            rt_method: "performance",
            persist: true,
            allow_held_key: true,
        });
      }
  }
  OrientationMouseRespKeyboardPlugin.info = info;

  return OrientationMouseRespKeyboardPlugin;

})(jsPsychModule);
