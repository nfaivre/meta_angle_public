var jsPsychOrientationMouse = (function (jspsych) {
  'use strict';

  const info = {
      name: "orientation-mouse",
      parameters: {
          /** A function with a single parameter that returns an HTML-formatted string representing the stimulus. */
          stim_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Stimulus function",
              default: undefined,
          },
		  /** A function with no parameter that returns the value of the correct answer. */
          correct_response_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Get response function",
              default: undefined,
          },
          /** The starting value of the stimulus parameter. */
          starting_value_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },

         /** The grating contrast. */
           adapted_constrast_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "At what contrast is the gabor",
                default: undefined,
         },

          /** The Angle condition. */
            angle_condition_function: {
                type: jspsych.ParameterType.FUNCTION,
                pretty_name: "What type of angle (without the jitter)",
                default: undefined,
            },

          /** To stock the future last orientation. */
          set_answer_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Bar final orientation",
              default: undefined,
          },
		  
          /** The text that appears on the button to finish the trial. */
          button_label: {
              type: jspsych.ParameterType.STRING,
              pretty_name: "Button label",
              default: "Continue",
          },
      },
  };
  /**
   * **orientation-mouse**
   *
   * jsPsych plugin for a orientation task where the subject recreates a stimulus from memory, using mouse
   *
   * @author Josh de Leeuw/Gabriel Wink
   * @see inspired by {@link https://www.jspsych.org/plugins/jspsych-reconstruction/ reconstruction plugin documentation on jspsych.org}
   */
  class OrientationMousePlugin {
      constructor(jsPsych) {
          this.jsPsych = jsPsych;
      }
      trial(display_element, trial)
	  {
          const draw = (param) => { // function
              //console.log(param);
              div_container.innerHTML = trial.stim_function(param);
          };
		  
		  const endTrial = () => { // function
              // measure response time
              if (FirstKeyPress == 0){
                FirstKeyPress = performance.now();
            }
              var endTime = performance.now();
              var response_time = Math.round(endTime - startTime);
              var RealResponseTime = Math.round(FirstKeyPress - startTime);
             
              // clear document event listeners
              document.removeEventListener("mouseup", mouseupevent);
			  document.removeEventListener("mousemove", resizeevent);
              // save data
              var trial_data = {
                  TypeOfAngle: trial.angle_condition_function(),
                  correct_response: trial.correct_response_function(),
                  response: param,
                  Err: trial.correct_response_function() - param,
                  Initial_position: trial.starting_value_function(),
                  RT: RealResponseTime,
                  AnswerDuration: response_time, 
                  Contrast: trial.adapted_constrast_function(),
                  
              };
              trial.set_answer_function(param);
              console.log( "Last answer: " + param ); 
              display_element.innerHTML = "";
              // next trial
              this.jsPsych.finishTrial(trial_data);
          };
		  
		  
          var FirstKeyPress = 0;
          var startTime = performance.now();
          // current param level
          var param = trial.starting_value_function();
		  var width = 300;
		  display_element.innerHTML = '<div id="jspsych-orientation-mouse-stim-container"><div id="orientation_container" '
										+ 'style="display: block; margin: auto; height: ' + width + 'px; width: ' + width + 'px; position: relative;"></div>'
										+ '<p> Click and drag the bar to change its orientation.<br /> When it matches the previous pattern, click ' + trial.button_label
										+ ' </p><a class="jspsych-btn" id="jspsych-orientation-mouse-btn">' + trial.button_label + '</a></div>';
          
		  display_element.querySelector("#jspsych-orientation-mouse-btn").addEventListener("click", endTrial);
		  
		  var div_container = document.getElementById("orientation_container");
          var dragging = false;
		  div_container.addEventListener("mousedown", (e) => {
				  e.preventDefault();
				  dragging = true;
			  });
			  
		  var mouseupevent = (e) => { dragging = false; };
          document.addEventListener("mouseup", mouseupevent);
          var resizeevent = (e) => {
              if (dragging) {
				  if (FirstKeyPress == 0){
					  FirstKeyPress = performance.now();
				  }
				  var bounds = div_container.getBoundingClientRect();
				  var mouseX = e.clientX - bounds.left - width * 0.5;
				  var mouseY = e.clientY - bounds.top - width * 0.5;
				  param = -Math.atan( mouseY / mouseX ) * 180 / Math.PI;
				  if(param < 0)
					  param += 180;
                  //console.log( "angle: " + param + ". X: " + mouseX + ". Y: " + mouseY );
				  draw(param);
              }
          };
          document.addEventListener("mousemove", resizeevent);
          draw(param);
      }
  }
  OrientationMousePlugin.info = info;

  return OrientationMousePlugin;

})(jsPsychModule);
