
///////////////////////////////////////// Metacognitive Part //////////////////////////////

var instructions_Metacog = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Explanation.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Next'],
  prompt: `
  <p><strong>Time to move on to the main experiment! </strong></p>
 
  <p>Your task remains the same: a grating pattern will appear on the screen. Focus on its <strong>orientation</strong>.	Then a white bar will appear: click on it and drag it until it reaches the same orientation than the previously seen pattern. Try to be as <strong>accurate</strong> as possible</p>

  <p>Once you have done <strong>several trials</strong> in a row, you will have to report how well you think you performed <strong>across these trials</strong>. Let's consider the mean target orientation, how precise were you in matching it? </p>
  
  
  <p>Play the video on the right to see the tutorial then press Next.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var instructions_Metacog_2 = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Example.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Got it!'],
  prompt: `
  <p>Use your mouse to adjust 
  <br/> 1 - the circle until the central bar orientation matches the <strong>mean target orientation</strong>.
  <br/> 2 - the red area until it fits your <strong> zone of confidence</strong> (= how far from the correct orientation you think you may have drifted in the previous trials). </p>
	<p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var instructions_Metacog_3 = {
  type: jsPsychVideoButtonResponse_DemoTask,
  stimulus: [
    'img/MetaCog_Example2.mp4'
  ],
  height:  screen.height*0.30, //950,
  choices: ['Got it!'],
  prompt: `
  <p>Easy right? Well, let's make it a little more difficult! </p>
  <p> Now <strong>2 grating patterns </strong> are going to appear each time. Remember <strong>both</strong>!
  <br/> The response bar  will appear next on one side of the screen to let you know <strong> which one is the real target </strong> (= the orientation you have to match). </p>
	<br/> Tips: the arrow that appear before the pattern will give you a cue, but not always a valid one!
  <p>Play the video on the right to see an example. 
  <br/> Click on the "I got it" button to give it a try.</p></div>
  `,
  response_allowed_while_playing: false,
  post_trial_gap: 1000, //Delay (in ms) of blank screen before we continue the flow.
  controls: true,
  autoplay: false,
};

var Begin_Metacog={
  type: jsPsychHtmlKeyboardResponse,
  stimulus: `
	<p><strong>Ready to go for real?</strong></p>
	<p>Press the space bar to start.</p>
  `,
  post_trial_gap: 1000 //Delay (in ms) of blank screen before we continue the flow.
};

function Metaconfiance(param){ // function used by answer.
	param = 90 - param;
  var html = '<img src="img/metaconfiance.png" style="position:absolute; width:100%; left:0px;" ><div class="circle">'
+'<div class="slice" style = "transform: rotate(90deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: scaleX(-1) rotate(90deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: rotate(270deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="slice" style = "transform: scaleX(-1) rotate(270deg) skewY(-' + ( param ) + 'deg);"> </div>'
+'<div class="lineMarker"> </div>'
+'<div>';
  return html;
}

function GetAngle90Random(){ return  1.5 ; } // Not relevant anymore since the starting value for the zone of confidance now depends on the participant reported mean target orientation but we still need a default value.

function Show_orientation_bar2(param){ // function used by answer.
  var html = '<img src="img/metaconfiance.png" style="position:absolute; width:100%; left:0px;'+
  'transform : rotate('+(-param)+'deg);" />';
  return html;
}

var metaconfiance2 = {
  type: jsPsychMetaconfiance_MeanSD,
  stim_orientation_function: Show_orientation_bar2,
  stim_metaconfiance_function: Metaconfiance,
  starting_value_orientation_function: GetAngleBar,
  starting_value_metaconfiance_function: GetAngle90Random,
data: {
      task: 'response'
    },
}
