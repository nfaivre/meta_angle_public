var jsPsychMetaconfiance_MeanSD = (function (jspsych) {
  'use strict';

  const info = {
      name: "metaconfiance_MeanSD",
      parameters: {
          /** A function with a single parameter that returns an HTML-formatted string representing the first stimulus. */
          stim_orientation_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Orientation Stimulus function",
              default: undefined,
          },
		  
		  /** A function with a single parameter that returns an HTML-formatted string representing the second stimulus. */
          stim_metaconfiance_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Metaconfiance Stimulus function",
              default: undefined,
          },

        
          /** The starting value of the bar orientation parameter. */
          starting_value_orientation_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },
		  
          /** The starting value of the metacognitive parameter. */
          starting_value_metaconfiance_function: {
              type: jspsych.ParameterType.FUNCTION,
              pretty_name: "Starting value function",
              default: undefined,
          },
		  
          /** The key to press for validating. */
          key_validate: {
            type: jspsych.ParameterType.KEY,
            pretty_name: "Key stop",
            default: ' ',
        },

          /** The HTML string to be displayed for the orientation phase. */
          stimulus_orientation_text: {
              type: jspsych.ParameterType.HTML_STRING,
              pretty_name: "Stimulus text",
              default: "1 - Click on the bar and drag it to change its orientation.<br /> When it matches the average orientation, press the space bar",
          },

          /** The HTML string to be displayed for the confidence. */
          stimulus_metaconfiance_text: {
              type: jspsych.ParameterType.HTML_STRING,
              pretty_name: "Stimulus text",
              default: "2- Click and drag to resize the portion of he circle corresponding to your zone of confidence.<br /> Then, press the space bar to continue",
          },

      },
  };
  /**
   * **metaconfiance_MeanSD**
   *
   * jsPsych plugin where the subject evaluate their performance, by setting a portion of a circle
   *
   * @author Josh de Leeuw/Gabriel Wink
   * @see inspired by {@link https://www.jspsych.org/plugins/jspsych-reconstruction/ reconstruction plugin documentation on jspsych.org}
   */
  class Metaconfiance_MeanSDPlugin {
      constructor(jsPsych) {
          this.jsPsych = jsPsych;
      }
      trial(display_element, trial)
	  {
		  var phase_orientation = true;
		  var orientation_choosen = 0;
		  var initial_orientation = trial.starting_value_orientation_function();
		  var initial_metaconfiance = trial.starting_value_metaconfiance_function();
		  
          const draw = (param) => { // function
              //console.log(param);
			  if( phase_orientation )
				div_container_internal.innerHTML = trial.stim_orientation_function(param);
			  else
				div_container_internal.innerHTML = trial.stim_metaconfiance_function(param);
          };
		  
      /* All the element we need when we end the trial*/
		  const endTrial = () => { // function
              // measure response time
              if (FirstKeyPress == 0){ // Currently, we should never encounter this case, as if the user doesn't answer, we call the modal instead of endTrial. But let's keep it, in case it changes.
                FirstKeyPress = performance.now();
			  }
              var endTime = performance.now();
              var response_time = Math.round(endTime - startTime);
              var RealResponseTime = Math.round(FirstKeyPress - startTime);
              var MetaconfianceTime = Math.round(endTime - MetaconfianceStart);
             
              // clear keyboard response
             this.jsPsych.pluginAPI.cancelKeyboardResponse(key_listener);
              // clear document event listeners
              document.removeEventListener("mouseup", mouseupevent);
			  document.removeEventListener("mousemove", resizeevent);
              // save data
              var Experiment_Stage="metacognition_judgment"
              var trial_data = {
                  ExpStage: Experiment_Stage,
                  metaconfiance: param,
                  Initial_position: initial_orientation,
                  ReportedMean: orientation_choosen,
                  RT: RealResponseTime,
                  MCT: MetaconfianceTime,
                  AnswerDuration: response_time,
              };
			  
              display_element.innerHTML = "";
              // next trial
              this.jsPsych.finishTrial(trial_data);
          };
		  
		  
          var FirstKeyPress = 0;
          var SecondKeyPress = 0;
          var MetaconfianceStart = 0;
          var startTime = performance.now();
          // current param level
          var param = initial_orientation;
		  var width = 300;
		  display_element.innerHTML = '<div id="myModal" class="modal"> <div class="modal-content"> <span class="close">&times;</span> <p id="modal_text">Please adjust the bar before pressing the space bar</p> </div></div>'
                                        +'<div id="jspsych-metaconfiance-stim-container"><div id="metaconfiance_container" '
										+ 'style="display: block; margin-left: auto; margin-right: auto;margin-top: -36px; height: ' + width + 'px; width: ' + width + 'px; position: relative;"><div id="metaconfiance_container_internal" style= "width:100%; height:100%;"></div></div>'
										+ '<p id="stimulus_text">' + trial.stimulus_orientation_text + '</p>' 
										+ '</div>';
          		  
		  var div_container = document.getElementById("metaconfiance_container");
		  var div_container_internal = document.getElementById("metaconfiance_container_internal");
          var dragging = false;
		  div_container.addEventListener("mousedown", (e) => {
				  e.preventDefault();
				  dragging = true;
			  });
			  
		  var mouseupevent = (e) => { dragging = false; };
          document.addEventListener("mouseup", mouseupevent);
          var resizeevent = (e) => {
              if (dragging) {
				  if (phase_orientation && FirstKeyPress == 0){
					  FirstKeyPress = performance.now();
				  }
				  if (phase_orientation == false && SecondKeyPress == 0){
					  SecondKeyPress = performance.now();
				  }
				  var bounds = div_container.getBoundingClientRect();
				  var mouseX = e.clientX - bounds.left - width * 0.5;
				  var mouseY = e.clientY - bounds.top - width * 0.5;
				  param = -Math.atan( mouseY / mouseX ) * 180 / Math.PI;
          if(param < 0)
					  param += 180;

				  if(orientation_choosen != 0)
				  {
					  param = param - orientation_choosen;
            if(param < 0)
					  param += 180;
				  }
				  if( phase_orientation == false && param > 90 ) //For this, we want a result between 0 and 90.
					param = 180 - param;
					
                  //console.log( "angle: " + param + ". X: " + mouseX + ". Y: " + mouseY ); // use to validate by clicking on the button
				  draw(param);
              }
          };
          document.addEventListener("mousemove", resizeevent);
          draw(param);

      const after_response = (info) => {
        if (this.jsPsych.pluginAPI.compareKeys(info.key, trial.key_validate)) {
          if ((phase_orientation && FirstKeyPress == 0) || (phase_orientation == false && SecondKeyPress == 0)) { // Get the modal
            var modal = document.getElementById("myModal");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // open the modal
            modal.style.display = "block";

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
              modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
              if (event.target == modal) {
                modal.style.display = "none";
              }
            }
          }
          else {
            if (phase_orientation) //End of the first phase, we start the second one.
            {
              phase_orientation = false;
              orientation_choosen = param;
              MetaconfianceStart = performance.now();
              // FirstKeyPress and feedback question...
              // FirstKeyPress = 0;
              div_container_internal.style.transform = 'rotate(' + (-orientation_choosen) + 'deg)';
              document.getElementById("modal_text").innerHTML = "Please adjust the confidence area before pressing the space bar";
              document.getElementById("stimulus_text").innerHTML = trial.stimulus_metaconfiance_text;
              param = initial_metaconfiance;
              draw(param);
            }
            else // True end
              endTrial();
          }
        }
      }

      var key_listener = this.jsPsych.pluginAPI.getKeyboardResponse({
        callback_function: after_response,
        valid_responses: [trial.key_validate],
        rt_method: "performance",
        persist: true,
        allow_held_key: true,
      });
    }
  }
  Metaconfiance_MeanSDPlugin.info = info;

  return Metaconfiance_MeanSDPlugin;

})(jsPsychModule);
