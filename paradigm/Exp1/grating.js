/***********************************************
/** Modified JGL
/***********************************************
/**
 * Make a sinusoidal grating. Creates a texture that later needs 
 * to be used with jglCreateTexture. 
 * Note: 0 deg means horizontal grating. 
 * If you want to ramp the grating with 
 * 2D Gaussian, also call function jglMakeGaussian and average the 
 * results of both functions
 * @param {Number} width: in pixels
 * @param {Number} height: in pixels
 * @param {Number} sf: spatial frequency in number of cycles per degree of visual angle
 * @param {Number} angle: in degrees
 * @param {Number} phase: in degrees 
 * @param {Number} pixPerDeg: pixels per degree of visual angle 
 * @memberof module:jglUtils
 */
function jglMakeGrating(width, height, sf, angle, phase, pixPerDeg) {

	// Get sf in number of cycles per pixel
	sfPerPix = sf / pixPerDeg;
	// Convert angle to radians
	angleInRad = ((angle + 0) * Math.PI) / 180;
	// Phase to radians
	phaseInRad = (phase * Math.PI) * 180;

	// Get x and y coordinates for 2D grating
	xStep = 2 * Math.PI / width;
	yStep = 2 * Math.PI / height;
	x = jglMakeArray(-Math.PI, xStep, Math.PI + 1); // to nudge jglMakeArray to include +PI
	y = jglMakeArray(-Math.PI, yStep, Math.PI + 1);
	// To tilt the 2D grating, we need to tilt
	// x and y coordinates. These are tilting constants.
	xTilt = Math.cos(angleInRad) * sf * 2 * Math.PI;
	yTilt = Math.sin(angleInRad) * sf * 2 * Math.PI;

	//What is width and height? Are these in degrees of visual angle or pixels?
	//See how lines2d and dots work. For example, jglFillRect(x, y, size, color) uses size in pixels
	//

	//How does jgl compute size in degress of visual angle
	var ixX, ixY; // x and y indices for arrays
	var grating = []; // 2D array
	for (ixX = 0; ixX < x.length; ixX++) {
		currentY = y[ixY];
		grating[ixX] = [];
		for (ixY = 0; ixY < y.length; ixY++) {
			grating[ixX][ixY] = Math.cos(x[ixX] * xTilt + y[ixY] * yTilt);
			// Scale to grayscale between 0 and 255
			grating[ixX][ixY] = Math.round(((grating[ixX][ixY] + 1) / 2) * 255);
		}
	}
	return (grating);
}

/**
 * Function to make array starting at low,
 * going to high, stepping by step.
 * Note: the last element is not "high" but high-step
 * @param {Number} low The low bound of the array
 * @param {Number} step the step between two elements of the array
 * @param {Number} high the high bound of the array
 */
function jglMakeArray(low, step, high) {
	if (step === undefined) {
		step = 1;
	}
	var size = 0
	var array = []
	if (low < high) {
		size = Math.floor((high - low) / step);
		array = new Array(size);
		array[0] = low;
		for (var i = 1; i < array.length; i++) {
			array[i] = array[i - 1] + step;
		}
		return array;
	} else if (low > high) {
		size = Math.floor((low - high) / step);
		array = new Array(size);
		array[0] = low;
		for (var j = 1; j < array.length; j++) {
			array[j] = array[j - 1] - step;
		}
		return array;
	}
	return [low];
}


function Canvas(id, back_id) {
	this.canvas = document.getElementById(id);
	this.context = this.canvas.getContext("2d"); // main on-screen context
	this.backCanvas = document.getElementById(back_id);
	this.backCtx = this.backCanvas.getContext("2d");
	this.height = $("#canvas").height(); // height of screen
	this.width = $("#canvas").width(); // width of screen
	document.getElementById(id).style.cursor = 'none';
}

function jglCreateTexture(canvas, array, mask, contrast) {

	/* Note on how imageData's work.
	 * ImageDatas are returned from createImageData,
	 * they have an array called data. The data array is
	 * a 1D array with 4 slots per pixel, R,G,B,Alpha. A
	 * greyscale texture is created by making all RGB values
	 * equals and Alpha = 255. The main job of this function
	 * is to translate the given array into this data array.
	 */
	if (!$.isArray(array)) {
		return;
	}
	var image;

	// 2D array passed in
	image = canvas.backCtx.createImageData(array.length, array.length);
	var row = 0;
	var col = 0;
	var colorValue = 0;
	for (var i = 0; i < image.data.length; i += 4) {
		mask_val = mask[row][col];
		ran_val = Math.random() * 256;
		colorValue = ran_val * (1 - contrast) + array[row][col] * contrast;
		image.data[i + 0] = colorValue;
		image.data[i + 1] = colorValue;
		image.data[i + 2] = colorValue;
		image.data[i + 3] = mask_val;
		col++;
		if (col == array[row].length) {
			col = 0;
			row++;
		}
	}
	return image;
}

// Create an image with mask and contrast of given color (array of r, g, b), on an alpha background. Can be put on top of something else.
function jglCreateMaskAndContrastTexture(canvas, color, mask, contrast) {
	var image;

	image = canvas.backCtx.createImageData(mask.length, mask.length);
	var row = 0;
	var col = 0;
	for (var i = 0; i < image.data.length; i += 4) {
		var ran_val = Math.random() * 256;
		var mask_val = mask[row][col];
		var mask_val_on_1 = mask_val / 255;
		image.data[i + 0] = ( ran_val * (1 - contrast) + color[0] * contrast ) * mask_val_on_1 + (color[0] * ( 1 - mask_val_on_1));
		image.data[i + 1] = ( ran_val * (1 - contrast) + color[1] * contrast ) * mask_val_on_1 + (color[1] * ( 1 - mask_val_on_1));
		image.data[i + 2] = ( ran_val * (1 - contrast) + color[2] * contrast ) * mask_val_on_1 + (color[2] * ( 1 - mask_val_on_1));
		image.data[i + 3] = Math.max(255 - ran_val, 255 - mask_val);
		col++;
		if (col == mask[row].length) {
			col = 0;
			row++;
		}
	}
	return image;
}


/***********************************************
/** Make Gaussian Mask
/***********************************************/

function twoDGaussian(amplitude, x0, y0, sigmaX, sigmaY, x, y) {
	var exponent = -((Math.pow(x - x0, 2) / (2 * Math.pow(sigmaX, 2))) + (Math.pow(y - y0, 2) / (2 *
		Math.pow(sigmaY, 2))));
	return amplitude * Math.pow(Math.E, exponent);
}

function make2dMask(width, height, amp, s) {
	var midX = Math.floor(width / 2)
	var midY = Math.floor(height / 2)
	var mask = []
	for (var i = 0; i < width; i++) {
		var col = []
		for (var j = 0; j < height; j++) {
			col.push(twoDGaussian(amp * 255, midX, midY, s, s, i, j))
		}
		mask.push(col)
	}
	return mask
}

function makeStim(canvas, backcanvas, angle, contrast, withPattern = true) {
	if(withPattern == false)
	{
		var size = document.getElementById(canvas).width;
		document.getElementById('canvas_container').innerHTML 
			= "<img src = 'img/circle.png' width ='" + size + "' height ='" + size + "' style='position:absolute; z-index:-50; "
			+  "transform: rotate(" + Math.random() * 360 + "deg);' >" + document.getElementById('canvas_container').innerHTML;
	}
	
	var jgl_canvas = new Canvas(canvas, backcanvas);
	var width = jgl_canvas.canvas.width * 0.864;
	var height = jgl_canvas.canvas.height * 0.864;
	// Create a circular mask
	var mask = make2dMask(jgl_canvas.canvas.width, jgl_canvas.canvas.height, 1, 85);
	var drawing;
	if(withPattern)
	{
		// Create a pixel array with the pattern
		var patternPixelArray = jglMakeGrating(width, height, 2, angle, 0, 0);
		// Create a texture by putting mask on the pixel array and adding contrast on everything.
		drawing = jglCreateTexture(jgl_canvas, patternPixelArray, mask, contrast);
	}
	else
	{
		var color = [0, 0, 0]; //Black
		var bodyColor = document.body.style.backgroundColor;
		if (bodyColor.indexOf('rgb') === 0)
		{
			color = bodyColor.match(/[\.\d]+/g).map(function (a) { return +a });
		}
		drawing = jglCreateMaskAndContrastTexture(jgl_canvas, color, mask, contrast);
	}
		
	jgl_canvas.context.putImageData(drawing, 0, 0);
}


function getEasyStim() {
	console.log( "Angle: " + anglePattern);
	makeStim('canvas1', 'backCanvas1', anglePattern, AdaptableContrast);
}

function getStimCircle() {
	makeStim('canvas1', 'backCanvas1', 0, AdaptableContrast, false);
}

function getHTMLCanvas() {
	return "<div class ='centerbox' id='canvas_container'> <canvas id ='canvas1' width ='579' height ='579' ></canvas> </div> <canvas id = backCanvas1></canvas></canvas>";
}


