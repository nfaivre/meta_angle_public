# Plot for Figure S4 (link between the two metacognitive reports)
          # 1. Organize data
## Bin per MetaError at the individual level
binning_MetaE<-Metacog_set %>% group_by(suj) %>% mutate(MetaError_dec = split_quantile(MetaError_z, 5))
BinnedData_MetaE<-binning_MetaE %>% group_by(suj, MetaError_dec) %>% mutate(MetaError_dec_Mean=mean(MetaError_z), 
                                                                            MetaAngle_z_dec=mean(MetaAngle_z))
BinnedData_MetaE<-BinnedData_MetaE[!duplicated(BinnedData_MetaE$MetaAngle_z_dec), ]
BinnedData_MetaE<-BinnedData_MetaE %>% group_by(suj) %>% mutate(SD_bin = ntile(SDset_z, n=2),
                                                                MeanErrorSet_bin = ntile(abs(MeanErrorSet), n=2))

# Bin at the group level
binning_Group_MetaE<-BinnedData_MetaE 
binning_Group_MetaE$MetaE_dec_group<-split_quantile(binning_Group_MetaE$MetaError_dec_Mean, 10)
BinnedData_group_MetaE<-binning_Group_MetaE %>% group_by(MetaE_dec_group) %>% mutate(MetaError_dec_Mean_group=mean(MetaError_dec_Mean))
BinnedData_group_MetaE$SD_bin<-as.factor(BinnedData_group_MetaE$SD_bin)
BinnedData_group_MetaE$MeanErrorSet_bin<-as.factor(BinnedData_group_MetaE$MeanErrorSet_bin)

# creation of a dataframe to plot data distribution as histogram
Histo_df<-Metacog_set
Histo_df<-subset(Histo_df,select = c("suj","Condition_Num", "MeanErrorSet","SDset_z","MetaError_z","MetaAngle_z"))

Histo_df<-Histo_df %>% group_by(suj) %>% mutate(AbsMean_cat = ntile(abs(MeanErrorSet), n=2),
                                                MetaError_z_cat = ntile(MetaError_z, n=2),
                                                SDset_z_cat = ntile(SDset_z, n=2))

Histo_df$AbsMean_cat = ifelse(Histo_df$AbsMean_cat == 1, "low mean error", "high mean error")
Histo_df$MetaError_z_cat = ifelse(Histo_df$MetaError_z_cat == 1,  "low MetaError", "high MetaError")
Histo_df$SDset_z_cat = ifelse(Histo_df$SDset_z_cat == 1,  "less variable", "more variable")

            # 2. Plot
##Plot MetaAngle ~ MetaError
Res1 <- conditional_effects(M2_extended, spaghetti = F, effect='MetaError_z')
Res1_plot <- Res1[[1]]
Res1_plot %>% ggplot(aes(x = MetaError_z,  y = estimate__, ymin = lower__, ymax = upper__)) +
  geom_ribbon(alpha=.2, color=NA) +
  geom_line(size=1) +
  labs(title="Data and model prediction", x = "MetaError (z-sf)", y = "MetaAngle (z-sf)") +
  xlim(-1, 2.5)+ ylim(-1, 1)+
  stat_summary(data=BinnedData_group_MetaE, 
               inherit.aes = F, 
               aes(MetaError_dec_Mean_group, MetaAngle_z_dec), 
               fun.data=mean_cl_normal, 
               alpha=.5, size =0.5)+
  theme_minimal()+
  theme_classic()+
  ggtitle("MetaAngle_z ~ MetaError_z")


##Plot MetaAngle ~ MetaError_z x MeanErrorSet^2 x SDset_z

ggplot(Histo_df, aes(x=MetaAngle_z, fill=MetaError_z_cat, color=MetaError_z_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.1)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2,4)+
  scale_color_brewer(palette="Set1")+
  scale_fill_brewer(palette="Set1")+
  facet_grid(AbsMean_cat~SDset_z_cat)

##Plot MetaAngle ~ MetaError_z x MeanErrorSet^2 x SDset_z x Cueing
Histo_df$AbsMean_cat = factor(Histo_df$AbsMean_cat, levels=c("low mean error", "high mean error"))
Histo_df_F<-Histo_df %>% filter(Condition_Num=="0") 
Histo_df_V<-Histo_df %>% filter(Condition_Num=="1") 
Histo_df_N<-Histo_df %>% filter(Condition_Num=="2") 
Histo_df_I<-Histo_df %>% filter(Condition_Num=="3") 


ggplot(Histo_df_F, aes(x=MetaAngle_z, fill=MetaError_z_cat, color=MetaError_z_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.1)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2,4)+
  scale_color_brewer(palette="Set1")+
  scale_fill_brewer(palette="Set1")+
  facet_grid(AbsMean_cat~SDset_z_cat)+
  ggtitle("F")

ggplot(Histo_df_V, aes(x=MetaAngle_z, fill=MetaError_z_cat, color=MetaError_z_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.1)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2,4)+
  scale_color_brewer(palette="Set1")+
  scale_fill_brewer(palette="Set1")+
  facet_grid(AbsMean_cat~SDset_z_cat)+
  ggtitle("V")

ggplot(Histo_df_N, aes(x=MetaAngle_z, fill=MetaError_z_cat, color=MetaError_z_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.1)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2,4)+
  scale_color_brewer(palette="Set1")+
  scale_fill_brewer(palette="Set1")+
  facet_grid(AbsMean_cat~SDset_z_cat)+
  ggtitle("N")

ggplot(Histo_df_I, aes(x=MetaAngle_z, fill=MetaError_z_cat, color=MetaError_z_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.1)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2,4)+
  scale_color_brewer(palette="Set1")+
  scale_fill_brewer(palette="Set1")+
  facet_grid(AbsMean_cat~SDset_z_cat)+
  ggtitle("I")
