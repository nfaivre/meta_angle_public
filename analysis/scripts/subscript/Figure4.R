# Figure 4 - Panel A
binning<-Metacog_set %>% group_by(suj, Orientation) %>% mutate(SD_bin = ntile(SDset_z, n=10), SD_dec = split_quantile(SDset_z, 10))
BinnedData<-binning %>% group_by(suj, Orientation, SD_dec) %>% mutate(SD_dec_Mean=mean(SDset_z), 
                                                                 MeanSet_dec=mean(MeanErrorSet), 
                                                                 MetaError_dec=mean(MetaError), 
                                                                 MetaAngle_z_dec=mean(MetaAngle_z))
BinnedData<-BinnedData[!duplicated(BinnedData$MetaAngle_z_dec), ]

## Bin according to SD at the group level
binning_Group<-BinnedData %>% group_by(Orientation) %>% mutate(SD_dec_group = split_quantile(SD_dec_Mean, 10))

BinnedData_group<-binning_Group %>% group_by(Orientation, SD_dec_group) %>% mutate(SD_dec_Mean_group=mean(SD_dec_Mean))


## Plot Model prediction + data for MetaAngle: SD x Orientation 
ResB <- conditional_effects(M2, spaghetti = F, effect='SDset_z:Orientation')
ResB_plot <- ResB[[1]]
ResB_plot %>% ggplot(aes(x = SDset_z,  y = estimate__, ymin = lower__, ymax = upper__, color = Orientation, fill = Orientation)) +
  geom_ribbon(alpha=.2, color=NA) +
  geom_line(size=1) +
  labs(title="Data and model prediction", x = "SD (z-tsf)", y = "MetaAngle (z-sf)") +
  xlab(bquote(SD[Set] (z-tsf)))+
  xlim(-1, 1)+ ylim(-1, 1)+
  stat_summary(data=BinnedData_group, 
               inherit.aes = F, 
               aes(SD_dec_Mean_group, MetaAngle_z_dec, group = Orientation, color = Orientation, 
               ), 
               fun.data=mean_cl_normal, 
               alpha=.5, size = .6)+
  theme_minimal()+
  theme_classic()+
  scale_color_brewer(palette="Paired")+
  scale_fill_brewer(palette="Paired")+
  ggtitle("MetaAngle_z ~ SDset_z x Orientation")



# Figure 4 - Panel B
Histo_df<-Metacog_set
Histo_df<-subset(Histo_df,select = c("suj","Orientation", "MeanErrorSet","SDset_z","MetaError_z","MetaAngle_z"))

Histo_df<-Histo_df %>% group_by(suj, Orientation) %>% mutate(AbsMean_cat = ntile(abs(MeanErrorSet), n=2),
                                                             SD_cat = ntile(SDset_z, n=2))

Histo_df$AbsMean_cat = ifelse(Histo_df$AbsMean_cat == 1, "low mean error", "high mean error")
Histo_df$SD_cat = ifelse(Histo_df$SD_cat == 1,  "less variable", "more variable")

ggplot(Histo_df, aes(x=MetaAngle_z, fill=SD_cat, color=SD_cat)) +
  geom_histogram(position="identity", alpha=0.6, binwidth=0.08)+
  #geom_vline(data=mu, aes(xintercept=grp.mean, color=SD_cat),linetype="dashed")+
  theme_minimal()+
  theme_classic()+
  xlim(-2.5,2.5)+
  facet_grid(AbsMean_cat~.)


# Figure 4 - Panel C

plot_model(M2,type='pred',terms=c('MeanErrorSet', 'SDset_z[-0.99,0.99]'))+
  xlab(bquote(MeanError[Set] (º)))+ ylab("MetaAngle (z-trsf)") +
  theme_classic()+
  scale_color_brewer(palette="Set2") + 
  xlim(-40,40)+
  ylim(-1,1.5)+
  ggtitle("Predicted values of the angle of the confidence zone")
