---
title: "MetaAction_WP3_Exp1.1"
output: 
  pdf_document:
    toc: yes
  html_document:
    theme: united
    toc: yes
    toc_float: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

## load libraries
libs2load = c('gridExtra','ggplot2','cowplot','emmeans','lme4','lmerTest','DHARMa','here','tidyverse','sjPlot','brms', 'car',"tidybayes", "RColorBrewer", "fabricatr")
lapply(libs2load, library, character.only = TRUE)

## Color set
theme_set(theme_bw())
 
scale_colour_discrete <-
  function(...) {
    scale_colour_brewer(..., palette = "Set2")
  }
scale_fill_discrete <-
  function(...) {
    scale_fill_brewer(..., palette = "Set2")
  }
```



## Locate individual data and define experiment parameters
```{r load, warning=F, message=F}

# Load participant data from Exp1.1 (orientation) - Downloaded from the Pavlovia server. 
ParticipantList = list.files(path=here::here('..','..','data'),pattern = 'csv', recursive = F)

tmp=list()
for (p in 1:length(ParticipantList)) {
 tmp[[p]] = read.csv(file=here::here('..','..','data',ParticipantList[p]),sep=",") %>% filter(task == 'response')
 tmp[[p]]$suj = paste('S',p,sep='_')
}
Data = bind_rows(tmp)
```


## Prepare Calibration and Metacog dataframes
```{r}

# 1. Extract demographic and info regarding the monitor and navigator used by the participant
    Data$NavigatorName[Data$NavigatorName==""] <- NA
       Nav<- na.omit(Data$NavigatorName)
     Data$Resolution[Data$Resolution==""] <- NA
       Res<- na.omit(Data$Resolution)
     Data$Answer[Data$Answer==""] <- NA
       Age_Sexe<- na.omit(Data$Answer)
       Age_Sexe <- matrix(Age_Sexe, nrow = length(unique(Data$suj)), byrow = TRUE)
 Nav_Res_Age_Sexe<-cbind(Nav, Res, Age_Sexe[,1], Age_Sexe[,2])

 Data %>% group_by(suj) %>% summarise(nav = unique(NavigatorName)) %>% filter(!is.element(nav, c('','NA')))
 Data %>% group_by(suj) %>% summarise(res = unique(Resolution)) %>% filter(!is.element(res, c('','NA')))
 Data %>% group_by(suj) %>% summarise(ans = unique(Answer)) %>% filter(!is.element(ans, c('','NA')))

# 2. Isolate calibration stage data
   # Warning: the first trial to be recorded is to be discard (it corresponds to the practice trial)
Calibration<-Data %>% filter(ExpStage == "calibration") %>% group_by(suj) %>% 
  mutate(Trial_Number=1:n(),TypeOfAngle = as.factor(TypeOfAngle)) %>% 
  select(suj,Trial_Number,TypeOfAngle,response,correct_response,Initial_position,RT,Contrast,Err, Flag)
Calibration_missed<-Calibration %>% filter(Flag == "on")
Calibration<-Calibration %>% filter(Flag == "off")
Calibration<-Calibration %>% filter(Trial_Number > 1) # 1st trial is a practice trial
Calibration$Orientation<-Calibration$TypeOfAngle
Calibration$Orientation = ifelse(Calibration$Orientation == "135" | Calibration$Orientation == "45", "oblique", "cardinal")
 
# 3. Isolate metacognitive stage data
  # Warning: the first set of trial to be recorded is to be discard (it corresponds to the practice set)
Metacog<-Data %>% filter(ExpStage == "metacognition_align") %>% group_by(suj) %>% 
  mutate(Trial_Number=1:n(),Seq_Number = rep(c(1:(max(Trial_Number)/4)),1,each=4),
         TypeOfAngle = as.factor(TypeOfAngle)) %>% 
  select(suj,Trial_Number,TypeOfAngle,response,correct_response,Initial_position,RT,Contrast,Seq_Number,Err, Flag, Contrast) 
  
  # extract the confidence report which occurs after 4 orientation matching trials. The variable Seq_Number increases every 4 trials
MetacogJudg<-Data %>% filter(ExpStage == "metacognition_judgment") %>% group_by(suj) %>% 
  mutate(Seq_Number=1:n(),TypeOfAngle = as.factor(TypeOfAngle), MetaAngle = metaconfiance) %>% 
  select(suj,Seq_Number,MetaAngle,ReportedMean)

  # fuse trial and sequence based dataframes
Metacog = full_join(Metacog,MetacogJudg,by=c('suj','Seq_Number'))
Metacog = Metacog %>% group_by(suj,Seq_Number) %>% mutate(Rep_Number = 1:n())

  # Assess the number of "missed trial" (Flag = on), relabel the whole set as missed and discard it from further analysis.
Metacog_missed<-Metacog %>% filter(Flag == "on") %>% mutate(FlagSet = "on") 
Metacog = full_join(Metacog,Metacog_missed[,c(1,9,15)],by=c('suj','Seq_Number'))
Metacog$FlagSet[is.na(Metacog$FlagSet)] <- "off"
Metacog<-Metacog %>% filter(FlagSet == "off")

  # Remove sets that include a trial that took longer than 9 sec.
Metacog_longer<-Metacog %>% filter(RT > 10000) %>% mutate(TooLong = "on") 
Metacog = full_join(Metacog,Metacog_longer[,c(1,9,16)],by=c('suj','Seq_Number'))
Metacog$TooLong[is.na(Metacog$TooLong)] <- "off"
Metacog<-Metacog %>% filter(TooLong == "off")

  # 1st sequence of trials is a practice one
Metacog<-Metacog %>% filter(Seq_Number > 1) 

  # for the 0º angle, if > 90, the reported mean needs to be transformed to set the maximal error at -90º or +90º to be comparable with the other Orientation orientations
Metacog$ReportedMean[Metacog$TypeOfAngle == 0] = ifelse(Metacog$ReportedMean[Metacog$TypeOfAngle == 0] > 90,
                                                        Metacog$ReportedMean[Metacog$TypeOfAngle == 0] - 180,
                                                        Metacog$ReportedMean[Metacog$TypeOfAngle == 0])

  # Encoding the Orientation orientation as a categorical variable (cardinal or oblique)
Metacog$Orientation<-Metacog$TypeOfAngle
Metacog$Orientation = ifelse(Metacog$Orientation == "135" | Metacog$Orientation == "45", "oblique", "cardinal")

  # Compute MetaError:
    # 'MetaError' variable to account for the absolute difference between the actual and reported mean target orientation  
Metacog = Metacog %>% mutate(MetaError = abs(as.numeric(as.character((TypeOfAngle))) - ReportedMean))

  # Compute mean error and standard deviation for each set of four trial
Metacog<-Metacog %>% group_by(suj,Seq_Number) %>% mutate(MeanErrorSet=mean(Err),SDset=sd(Err)) 
Metacog_set = Metacog %>% filter(Rep_Number==1) %>%
  select(suj,TypeOfAngle,Orientation, Seq_Number,MetaAngle,ReportedMean,MeanErrorSet,SDset,MetaError, Contrast)

  # Z-transformation of the variable of interest - The MeanErrorSet is not z-tsf since it can be positive or negative, z-transforming it would mean losing its meaning 
Metacog_set = Metacog_set %>% group_by(suj) %>% 
  mutate(SDset_z = scale(SDset), MetaAngle_z = scale(MetaAngle), MetaError_z = scale(MetaError))

Metacog = Metacog %>% group_by(suj) %>%  
  mutate(Err_z = scale(Err), Abs_Err=abs(Err), Abs_Err_z = scale(abs(Err)), invRT_z = scale(1/RT))

# For the position effect we need to work with the single trial performances (level1) and not only the set-related performances. 
Metacog_test<-Metacog %>%
  select(suj, Seq_Number, Rep_Number, Err, Abs_Err, RT)
Metacog_test<-Metacog_test %>% 
  mutate(invRT= 1/RT)

# We renumber the repetition number simply to have a meaningful intercept in our latter analysis of the position effect.
Metacog_test$Rep_Number = ifelse(Metacog_test$Rep_Number == 1, 0, Metacog_test$Rep_Number)
Metacog_test$Rep_Number = ifelse(Metacog_test$Rep_Number == 2, 1, Metacog_test$Rep_Number)
Metacog_test$Rep_Number = ifelse(Metacog_test$Rep_Number == 3, 2, Metacog_test$Rep_Number)
Metacog_test$Rep_Number = ifelse(Metacog_test$Rep_Number == 4, 3, Metacog_test$Rep_Number)


#
#
#
#
#
#
#
# Creation of a summary table for description of raw data
    # It is an necessary steps because not all participants have the same number of trials (see trial exclusion criteria). 
MetacogSummary<-Metacog %>% group_by(suj, Orientation) %>% mutate(RT_Tot=mean(RT), MeanErrorSetTot=mean(abs(Err)),SDsetToT=sd(Err), MetATot = mean(MetaAngle), MetaETot = mean(abs(MetaError))) 
MetacogSummary_set = MetacogSummary %>% filter(Rep_Number==1) %>%
  select(suj,Orientation, RT_Tot, Contrast, MeanErrorSetTot, SDsetToT, MetATot, MetaETot)
MetacogSummary_set<-MetacogSummary_set[!duplicated(MetacogSummary_set$MeanErrorSetTot), ]
```

# Make some plots to check the quality of the data
```{r}
## 1 - Calibration step
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"Exp1.1_PlotCalibration.R"), print. = TRUE)

## 2 - Metacognitive step
source(file = file.path(code.dir,"Exp1.1_PlotMetacog.R"), print. = TRUE)

```


# Participants level 1 performances
```{r}
## 3 - Summary of descriptive statistics
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"Exp1.1_SummaryDescriptiveStatistics.R"), print. = TRUE)
# the results of this analysis can be found in supplementary materials
```


# Main Bayesian model with MetaError (|actual - reported Orientation orientation|)
```{r brms, echo=F}
# Model M1 definition
priors = c(prior(normal(0,2), class = b)) # default priors

M1 <- brm(MetaError_z ~ poly(MeanErrorSet,2)*SDset_z * Orientation + (poly(MeanErrorSet,2)+SDset_z+Orientation|suj), 
              data=Metacog_set, 
              sample_prior = T, prior = priors, chains = 4, family=gaussian,iter = 2000, cores = 4,
              file=here::here('..','models','M1'),file_refit='on_change')
summary(M1)

# extract posteriors
post = posterior_samples(M1) %>% select(contains('b_')) %>% pivot_longer(cols=everything())
post %>%  ggplot(aes(x = value, y = 0)) + stat_halfeye() + facet_wrap(~name,scales='free')

# Compute BF for the main hypothesis (non-directional, type "there is no effect of this factor") 
### Main variables
h <- c("MeanErrorSet1" = "polyMeanErrorSet21  = 0")
H1<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

h <- c("MeanErrorSet2" = "polyMeanErrorSet22  = 0")
H2<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

h <- c("SDset" = "SDset_z  = 0")
H3<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

h <- c("Orientation" = "Orientationoblique  = 0")
H4<-(hyp=hypothesis(M1, h))$hypothesis[1,6]


### Interaction (2 factors)
 h <- c("MeanErrorSet1 x SD" = "polyMeanErrorSet21:SDset_z  = 0")
H5<-(hyp=hypothesis(M1, h))$hypothesis[1,6]
 h <- c("MeanErrorSet2 x SD" = "polyMeanErrorSet22:SDset_z  = 0")
H6<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x Orientation" = "polyMeanErrorSet21:Orientationoblique  = 0")
H7<-(hyp=hypothesis(M1, h))$hypothesis[1,6]
h <- c("MeanErrorSet2 x Orientation" = "polyMeanErrorSet22:Orientationoblique  = 0")
H8<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

h <- c("Orientation x SD" = "SDset_z:Orientationoblique  = 0")
H9<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

### Interaction (3 factors)
h <- c("MeanErrorSet1 x SD x Orientation" = "polyMeanErrorSet21:SDset_z:Orientationoblique  = 0")
H10<-(hyp=hypothesis(M1, h))$hypothesis[1,6]
h <- c("MeanErrorSet2 x SD x Orientation" = "polyMeanErrorSet22:SDset_z:Orientationoblique  = 0")
H11<-(hyp=hypothesis(M1, h))$hypothesis[1,6]

Evid.Ratio_M1<-rbind(H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11)

```

# Main Bayesian model with MetaAngle (angle of the zone of confidence) 
```{r}

# Model M2 definition
priors = c(prior(normal(0,2), class = b)) 

M2 <- brm(MetaAngle_z ~ poly(MeanErrorSet,2) * SDset_z * Orientation + (poly(MeanErrorSet,2)+SDset_z+Orientation|suj), 
              data=Metacog_set, 
              sample_prior = T, prior = priors, chains = 4, family=gaussian,iter = 2000, cores = 4,
              file=here::here('..','models','M2'),file_refit='on_change')
summary(M2)

# extract posteriors
post = posterior_samples(M2) %>% select(contains('b_')) %>% pivot_longer(cols=everything())
post %>% ggplot(aes(x = value, y = 0)) + stat_halfeye() + facet_wrap(~name,scales='free')

# Compute BF for the main hypothesis (non-directional, type "there is no effect of this factor") 
### Main variables
h <- c("MeanErrorSet1" = "polyMeanErrorSet21  = 0")
H1<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

h <- c("MeanErrorSet2" = "polyMeanErrorSet22  = 0")
H2<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

h <- c("SDset" = "SDset_z  = 0")
H3<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

h <- c("Orientation" = "Orientationoblique  = 0")
H4<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

### Interaction (2 factors)
 h <- c("MeanErrorSet1 x SD" = "polyMeanErrorSet21:SDset_z  = 0")
H5<-(hyp=hypothesis(M2, h))$hypothesis[1,6]
 h <- c("MeanErrorSet2 x SD" = "polyMeanErrorSet22:SDset_z  = 0")
H6<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x Orientation" = "polyMeanErrorSet21:Orientationoblique  = 0")
H7<-(hyp=hypothesis(M2, h))$hypothesis[1,6]
h <- c("MeanErrorSet2 x Orientation" = "polyMeanErrorSet22:Orientationoblique  = 0")
H8<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

h <- c("Orientation x SD" = "SDset_z:Orientationoblique  = 0")
H9<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

### Interaction (3 factors)
h <- c("MeanErrorSet1 x SD x Orientation" = "polyMeanErrorSet21:SDset_z:Orientationoblique  = 0")
H10<-(hyp=hypothesis(M2, h))$hypothesis[1,6]
h <- c("MeanErrorSet2 x SD x Orientation" = "polyMeanErrorSet22:SDset_z:Orientationoblique  = 0")
H11<-(hyp=hypothesis(M2, h))$hypothesis[1,6]

Evid.Ratio_M2<-rbind(H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11)

```


# Effect of trial number (position within the set) - Using individual set error regression
```{r}
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"Exp1.1_PositionEffect.R"), print. = TRUE)
```



# Relationship MetaAngle/MetaError
```{r}
# Model M2_extended definition
priors = c(prior(normal(0,2), class = b)) # set a N(0,2) prior on everything for now, but will have to change
M2_extended <- brm(MetaAngle_z ~ MetaError_z*poly(MeanErrorSet,2)*SDset_z * Orientation + (MetaError_z+poly(MeanErrorSet,2)+SDset_z+Orientation|suj), data=Metacog_set, 
            sample_prior = T, prior = priors, chains = 4, family=gaussian,iter = 2000, cores = 4,
            file=here::here('..','models','M2_extended'),file_refit='on_change')
summary(M2_extended)

# extract posteriors
post = posterior_samples(M2_extended) %>% select(contains('b_')) %>% pivot_longer(cols=everything())
post %>%  ggplot(aes(x = value, y = 0)) + stat_halfeye() + facet_wrap(~name,scales='free')

# Compute BF for the main hypothesis (non-directional, type "there is no effect of this factor") 
### Main variables
h <- c("MetaError" = "MetaError_z  = 0")
H1<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet1" = "polyMeanErrorSet21  = 0")
H2<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2" = "polyMeanErrorSet22  = 0")
H3<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("SDset" = "SDset_z  = 0")
H4<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("Orientation" = "Orientationoblique  = 0")
H5<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

### Interaction (2 factors)
h <- c("MetaError x MeanErrorSet1" = "MetaError_z:polyMeanErrorSet21  = 0")
H6<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MetaError x MeanErrorSet2" = "MetaError_z:polyMeanErrorSet22  = 0")
H7<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MetaError x SD" = "MetaError_z:SDset_z  = 0")
H8<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x SD" = "polyMeanErrorSet21:SDset_z  = 0")
H9<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x SD" = "polyMeanErrorSet22:SDset_z  = 0")
H10<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MetaError x orientation" = "MetaError_z:Orientationoblique  = 0")
H11<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x Orientation" = "polyMeanErrorSet21:Orientationoblique  = 0")
H12<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x Orientation" = "polyMeanErrorSet22:Orientationoblique  = 0")
H13<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("Orientation x SD" = "SDset_z:Orientationoblique  = 0")
H14<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

### Interaction (3 factors)
h <- c("MeanErrorSet1 x SD x MetaE" = "MetaError_z:polyMeanErrorSet21:SDset_z  = 0")
H15<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x SD x MetaE" = "MetaError_z:polyMeanErrorSet22:SDset_z  = 0")
H16<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x MetaE x Orientation" = "MetaError_z:polyMeanErrorSet21:Orientationoblique  = 0")
H17<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x MetaE x Orientation" = "MetaError_z:polyMeanErrorSet22:Orientationoblique  = 0")
H18<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("Orientation x SD x MetaE" = "MetaError_z:SDset_z:Orientationoblique  = 0")
H19<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet1 x SD x Orientation" = "polyMeanErrorSet21:SDset_z:Orientationoblique  = 0")
H20<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x SD x Orientation" = "polyMeanErrorSet22:SDset_z:Orientationoblique  = 0")
H21<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

### Interaction (4 factors)
h <- c("MeanErrorSet1 x MetaE x SD x Orientation" = "MetaError_z:polyMeanErrorSet21:SDset_z:Orientationoblique  = 0")
H22<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

h <- c("MeanErrorSet2 x MetaE x SD x Orientation" = "MetaError_z:polyMeanErrorSet22:SDset_z:Orientationoblique = 0")
H23<-(hyp=hypothesis(M2_extended, h))$hypothesis[1,6]

Evid.Ratio_M2_extended<-rbind(H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15, H16, H17, H18, H19, H20, H21, H22, H23)

```

## Plots for manuscript
```{r}
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"Figure3.R"), print. = TRUE)
source(file = file.path(code.dir,"Figure4.R"), print. = TRUE)

```

## Plots for supplementary materials
```{r}
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"FigureS1.R"), print. = TRUE)
source(file = file.path(code.dir,"FigureS2.R"), print. = TRUE)

```



## Models with informed priors from pilot
```{r}
code.dir <- here::here('.','Cleaned/subscript')
source(file = file.path(code.dir,"Exp1.1_InformedPriors.R"), print. = TRUE)
source(file = file.path(code.dir,"Figure3_InformedPrior.R"), print. = TRUE)


```



Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
