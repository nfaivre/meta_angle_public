Experimental paradigm, data, and analysis scripts corresponding to: 

Marie Chancel, Elisa Filevich, Nathan Faivre. Exogenous and endogenous sources of uncertainty inform global performance monitoring. PsyArXi 


./paradigm contains the code to run the experimental paradigm
./data contains the individual data files
./analysis/scripts contains the code to analyse the data

https://osf.io/w845z
https://gitlab.com/nfaivre/meta_angle_public
